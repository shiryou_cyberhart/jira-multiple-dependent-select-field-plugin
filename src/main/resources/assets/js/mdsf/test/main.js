$(document).ready(function() {

  var html = '<table>' +
    '<tr>' + 
      '<td>' +
        '<select id="selectA">' +
          '<option value="-1">None</option>' +
          '<option value="1000" selected="selected">Number</option>' +
          '<option value="1001">Character</option>' +
        '</select>' +
      '</td>' +
      '<td>' +
        '<select id="selectB">' +
          '<option value="1002" selected="selected">&lt;10</option>' +
          '<option value="1003">&lt;100</option>' +
        '</select>' +
      '</td>' +
      '<td>' +
        '<select id="selectC">' +
          '<option value="1006" selected="selected">1</option>' +
          '<option value="1007">2</option>' +
        '</select>' +
      '</td>' +
    '</tr>' +
  '</table>';

  var combos = [
    [
      ["-1", "None"],
      ["-1", "None"],
      ["-1", "None"]
    ],
    [
      ["1000", "Number"],
      ["1002", "<10"],
      ["1006", "1"]
    ],
    [
      ["1000", "Number"],
      ["1002", "<10"],
      ["1007", "2"]
    ],
    [
      ["1000", "Number"],
      ["1003", "<100"],
      ["1008", "50"]
    ],
    [
      ["1001", "Character"],
      ["1004", "A-M"],
      ["1009", "a"]
    ],      
    [
      ["1001", "Character"],
      ["1005", "N-Z"],
      ["1010", "s"]
    ],    
    [
      ["1001", "Character"],
      ["1005", "N-Z"],
      ["1011", "v"]
    ]
  ];

  test("Select a column to re-populate all columns from its right", 2, function() {

    $("#qunit-fixture").append(html);

    var $selectB = $("#selectB");
    $selectB.find('option[value=1002]').removeAttr('selected');
    $selectB.find('option[value=1003]').attr('selected', 'selected');
    Mdsf.updateRow($selectB.get(0), combos);

    equal($('#selectC').children().size(), 1, "Only one option for Number < 100");
    equal($('#selectC').val(), 1008, "Only one option for Number < 100, which is 50");
  });

  test("Select the left-most column to None should collaspe every thing", 2, function() {

    $("#qunit-fixture").append(html);

    var $selectA = $("#selectA");
    $selectA.find('option[value=1000]').removeAttr('selected');
    $selectA.find('option[value=-1]').attr('selected', 'selected');
    Mdsf.updateRow($selectA.get(0), combos);

    equal($('#selectB').children().size(), 1, "Only one option left for column B which is None");
    equal($('#selectC').children().size(), 1, "Only one option left for column C which is None");
  });

});
