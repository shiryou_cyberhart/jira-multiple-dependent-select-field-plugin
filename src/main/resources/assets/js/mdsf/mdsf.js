(function() {

  var root = this;

  var _ = root._;
  var $ = root.jQuery || root.$;

  var Mdsf = root.Mdsf = {

    _isOption: function(option) {
      return option.nodeName && option.nodeName == "OPTION";
    },

    _addOption: function(select, option) {

      if (!this._isOption(option)) {
        // This is a legacy behavior that we don't really need to support. Just demonstrating.
        option = new Option(option[1], option[0]);
      }

      if (_.find(select.options, function(e) { return e.value == option.value; })) {
        // Same option shouldn't be added twice.
        return;
      } else {
        $(option).appendTo(select);
      }
    },

    _updateRowByCombo: function(cells, columns, combo) {
      _.each(columns, function(column) {
        Mdsf._addOption(cells[column].children[0], new Option(combo[column][1], combo[column][0]));
      });
    },

    updateRow: function(that, combos) {

      var $cells = $(that).parent().parent().children();

      var thatColumn = 0;
      // The columns from the left to "that" (trigger)
      var noUpdateColumns = [];
      // Selected values from left to "that"
      var noUpdateValues = [];
      // The rest of the columns
      var updateColumns = [];

      // First we find the ID of the parent (column ID from the left)
      for (var i = 0; i < $cells.size(); i++) {
        if ($cells.get(i).children[0].id == that.id) {
          thatColumn =  i;
          break;
        }
      }

      // Then we address what update and what not to
      _.each($cells.get(), function(e, i) {
        var select = e.children[0];
        var selectedIndex = select.selectedIndex;
        if (i <= thatColumn && selectedIndex != -1) {
          noUpdateColumns.push(i);
          noUpdateValues.push($(select).val());
        } else {
          updateColumns.push(i);
        }
      });

      // Then we remove all the children
      _.each(updateColumns, function(e) {
        $($cells.get(e)).find("select").empty();
      });

      // Spawn according to "combos"
      _.each(combos, function(combo, i) {

        var match = true;
        for (var j = 0; j < noUpdateColumns.length; j++) {

          if (combo[noUpdateColumns[j]][0] != noUpdateValues[j]) {
            match = false;
          }
        }

        if (match) {
          Mdsf._updateRowByCombo($cells.get(), updateColumns, combo);
        } else {
          // This happens if combo has inconsistent values with those values of non-updating columns.
        }
      });
    }
  };


}).call(this);
