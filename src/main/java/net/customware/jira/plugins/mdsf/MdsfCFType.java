package net.customware.jira.plugins.mdsf;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.customfield.ProjectImportableCustomField;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.MultipleCustomFieldType;
import com.atlassian.jira.issue.customfields.MultipleSettableCustomFieldType;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.StringCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.atlassian.sal.api.ApplicationProperties;
import com.google.common.base.Predicate;
import com.opensymphony.util.TextUtils;
import org.ofbiz.core.entity.GenericValue;

import java.util.*;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.filterKeys;
import static java.lang.String.format;
import static net.customware.jira.plugins.mdsf.MdsfCFType.Utils.removeAdditionalValidationParameters;

/**
 * Multiple Dependent Select Field (MDSF) fields resemble Cascading
 * Select fields but support more than two levels of select fields.
 *
 * The Transport class is a single instance of the {@link Combo} class.
 */
public class MdsfCFType extends StringCFType implements MultipleSettableCustomFieldType, MultipleCustomFieldType, ProjectImportableCustomField, SortableCustomField<Combo> {

    public static class Utils {

        /**
         * This is a hack to avoid this field to break due to additional validation parameters added in JIRA 6.2 (which
         * is meant for {@link com.atlassian.jira.issue.customfields.impl.UserCFType}.
         *
         * @see com.atlassian.jira.issue.fields.CustomFieldImpl# addAdditionalParametersForValidation(com.atlassian.jira.issue.customfields.view.CustomFieldParams, com.atlassian.jira.issue.Issue)
         * @param customFieldParams
         * @return
         */
        public static Collection<String> removeAdditionalValidationParameters(Map<String, Collection<String>> customFieldParams) {

            Map<String, Collection<String>> m = filterKeys(customFieldParams, new Predicate<String>() { public boolean apply(String item) {
                return !isAdditionalValidationParameter(item);
            }});

            return newArrayList(concat(m.values()));
        }

        private static boolean isAdditionalValidationParameter(String item) {
            return null != item && (item.equals("issue") || item.equals("requireProjectIds"));
        }

    }

    // TextCFType implements SortableCustomField as a String so have to extend from before that
    private final OptionsManager optionsManager;
    private final CustomFieldManager customFieldManager;
    private final ApplicationProperties applicationProperties;
    private final ProjectCustomFieldImporter projectCustomFieldImporter;

    public MdsfCFType(CustomFieldValuePersister customFieldValuePersister,
                      GenericConfigManager genericConfigManager,
                      final OptionsManager optionsManager,
                      final CustomFieldManager customFieldManager,
                      final ApplicationProperties applicationProperties) {
	super(customFieldValuePersister, genericConfigManager);
        this.optionsManager = optionsManager;
        this.customFieldManager = customFieldManager;
        this.applicationProperties = applicationProperties;
        this.projectCustomFieldImporter = new MdsfCustomFieldImporter(optionsManager);
    }

    /**
     * This is where the different aspects of a custom field such as
     * having a default value or other config items is set.
     */
    @Override
    public List<FieldConfigItemType> getConfigurationItemTypes()
    {
        // Start from an empty list so there is no default value config item
        //final List<FieldConfigItemType> configurationItemTypes = new ArrayList<FieldConfigItemType>();

        // Note that the default will likely want some fields defining first
        final List<FieldConfigItemType> configurationItemTypes = super.getConfigurationItemTypes();
        configurationItemTypes.add(new MdsfConfigItem(this, optionsManager, customFieldManager, applicationProperties));

        return configurationItemTypes;
    }

    @Override
    protected PersistenceFieldType getDatabaseType()
    {
        return PersistenceFieldType.TYPE_LIMITED_TEXT;
    }

    @Override
    public void createValue(CustomField field, Issue issue, Object value)
    {
        customFieldValuePersister.createValues(field, issue.getId(), getDatabaseType(), EasyList.build(getDbValueFromObject(value)));

        //Assign Mdsf Value to associate select field
        if (MdsfDAO.getPopulateToDependentField(field.getRelevantConfig(issue))){
            String [] combination = ((Combo)value).getRow();
            for(int i = 0; i < combination.length; i++)
            {
                updateSelectField(issue, ((Combo)value).getCurrentfields().get(i), combination[i]);
            }
        }
    }

    @Override
    public void updateValue(CustomField field, Issue issue, Object value)
    {
        customFieldValuePersister.updateValues(field, issue.getId(), getDatabaseType(), EasyList.build(getDbValueFromObject(value)));

        //Assign Mdsf Value to associate select field
        if (MdsfDAO.getPopulateToDependentField(field.getRelevantConfig(issue))){
            String [] combination = ((Combo)value).getRow();
            for(int i = 0; i < combination.length; i++)
            {
                updateSelectField(issue, ((Combo)value).getCurrentfields().get(i), combination[i]);
            }
        }
    }

    @Override
    public Set remove(final CustomField field)
    {
        final Set issues = super.remove(field);
        optionsManager.removeCustomFieldOptions(field);
        return issues;
    }

    /**
     * Given a string entered by a user, get the corresponding Combo object.
     * This object is available in the view velocity template as
     * $value and is not necessarily a String.
     *
     * @param string the contents of this custom field as displayed
     * @return the Combo object
     */
    public Object getSingularObjectFromString(final String string) throws FieldValidationException
    {
        log.warn("getSingularObjectFromString: EMPTY fieldConfig, with string: " + string);
        return getSingularObjectFromString(string, null);
    }

    public Object getSingularObjectFromString(final String string, FieldConfig fieldConfig) throws FieldValidationException
    {
        log.debug("getSingularObjectFromString: string passed from presentation layer: " + string);
        if (fieldConfig == null) {
            // This field may not be valid in the current project
            log.warn("getSingularObjectFromString: directly called with EMPTY fieldConfig, with string: " + string);
            throw new FieldValidationException("No field configuration found, so values cannot be ordered properly for display");
        }

        // This methods happens to do the same this as converting from a
        // database string representation
        return getObjectFromDbValue(string, fieldConfig);
    }

    /**
     * Convert a Combo object to the string that is displayed during
     * editing an issue.
     * This object is available in the edit velocity template as
     * $value and is a String.
     *
     * @param value a Combo object
     * @return the string that represents the contents of this field
     */
    public String getStringFromSingularObject(final Object value)
    {
        log.debug("getStringFromSingularObject: combo: " + value);
        assertObjectImplementsType(Combo.class, value);
        Combo combo = (Combo)value;
        return combo.toString();
    }

    /**
     * Convert a Combo object into the option value (a string) used in
     * the database.  The string for an MDSF option looks like:
     * customfield_10000 10002 customfield_10001 10004 customfield_10002 10011
     * with the dependent custom field ids sorted and the option id for each
     * value in each dependent field separated by a space.
     */
    protected Object getDbValueFromObject(final Object customFieldObject)
    {
        log.debug("getDbValueFromObject: combo: " + customFieldObject);
        if (customFieldObject == null) {
            // This occurs when removeValue calls updateValue with a null as the new value
            return "";
        }
        Combo combo = (Combo)customFieldObject;
        String value = MdsfDAO.convertMapToOptionString(combo.getMap());
        return value;
    }

    @Override
    protected Object getObjectFromDbValue(final Object databaseValue) throws FieldValidationException
    {
        log.warn("getObjectFromDbValue: EMPTY fieldConfig, with string: " + databaseValue);
        return getObjectFromDbValue(databaseValue, null);
    }

    // TODO why is this called 4 times to display an issue? Or is that
    // just twice? Once for checking if the value is non-null in
    // StandardFieldScreenRendererFactory.java
    // getCustomFieldRenderLayoutItem
    protected Object getObjectFromDbValue(final Object databaseValue, FieldConfig fieldConfig) throws FieldValidationException
    {
        // View issue: first calls CustomFieldImpl.getValue() which
        // calls this.getValueFromIssue() which calls this method
        // Edit issue: see getValueFromCustomFieldParams()
        log.debug("getObjectFromDbValue: string: " + databaseValue);
        if (fieldConfig == null) {
            // This field may not be valid in the current project
            log.warn("getObjectFromDbValue: directly called with EMPTY fieldConfig, with string: " + databaseValue);
            throw new FieldValidationException("No field configuration found, so values cannot be ordered properly for display");
        }

        String databaseValueStr = (String)databaseValue;

        // TODO how is it non null? What is expected in custom fields. is not clearly documented around this area
        if (databaseValueStr == null || databaseValueStr.trim().equals("")) {
            log.debug("No database value has been stored yet");
            return null;
        }

        Map<String, String> valuesMap = MdsfDAO.convertOptionStringToMap(databaseValueStr);
        if (valuesMap == null) {
            throw new FieldValidationException("Could not parse: " + databaseValue);
        }
        Combo combo = new Combo(valuesMap);
        combo.setCurrentfields(MdsfDAO.getCurrentFields(fieldConfig));

        // For searching
        String value = MdsfDAO.convertMapToOptionString(valuesMap);
        final Options options = optionsManager.getOptions(fieldConfig);
        Option comboOption = options.getOptionForValue(value, null);
        if (comboOption == null) {
            throw new FieldValidationException("Could not find an option: " + value);
        }
        combo.setOptionid(comboOption.getOptionId());

        return combo;
    }

    /**
     * This method is the same as the one in AbstractSingleFieldType
     * except that the call to getObjectFromDbValue passes in a
     * FieldConfig object, plus the MLCS connection work.
     * This method is also called by CustomFieldImpl getValue()
     *
     * @return a Combo object
     */
    @Override
    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        log.debug("getValueFromIssue: " + issue + ", field: " + field.getName());
        if (issue == null || issue.getKey() == null) {
            // The issue is being created
            FieldConfig fieldConfig = field.getRelevantConfig(issue);
            return getDefaultValue(fieldConfig);
        }

        final List values = customFieldValuePersister.getValues(field, issue.getId(), getDatabaseType());

        if (values == null || !isFieldConfiged(field, issue)) {
            return null;
        }

        if (values.isEmpty()) {
            // See if there is an old MLCS field that has data to be converted for this field and issue.
            try {
                return getComboFromMLCS(issue, field, true);
            } catch (Exception ex) {
                // TODO why would getMlcsField raise an NPE?
                log.info("While checking for MLCS field contents: " + ex.getMessage(), ex);
                return null;
            }
        } else if (values.size() > 1) {
            log.error("More than one value stored for custom field id '" + field.getId() + "' for issue '" + issue.getKey() + "'.  Values " + values);
            return null;
        } else {
            Object databaseValue = values.get(0);
            if (databaseValue == null) {
                return null;
            }
            try {
                // If the field is not valid for the issue, the field config will be null
                FieldConfig fieldConfig = field.getRelevantConfig(issue);
                return getObjectFromDbValue(databaseValue, fieldConfig);
            } catch (FieldValidationException e) {
                // Create the log message
                String message = "Issue " + issue.getKey() + " has an invalid value '" + databaseValue + "' stored in the field '" + field.getName() + "'.";
                if (log.isDebugEnabled()) {
                    // Still log a stacktrace at the debug level just in case we REALLY want it.
                    log.debug(message, e);
                } else {
                    // Simple warning with no stack trace, because this can happen easily in JIRA. eg see JRA-15424.
                    log.warn(message + ' ' + e.getMessage());
                }
                return null;
            }
        }
    }

    public String availableForBulkEdit(final BulkEditBean bulkEditBean) {
        // return null;// TODO when supported
        return "MDSF fields do not support bulk move yet";
    }

    /**
     * Find the MLCS value for the given issue, if any.  If necessary,
     * create new option values in the MDSF field's dependent fields,
     * and create a new Combo for the MDSF field. After the new
     * options are created, the MLCS data is removed from the
     * database for this issue.
     *
     * @return the Combo that represents the MLCS value, or null if not found
     */
    private Combo getComboFromMLCS(Issue issue, CustomField field, boolean truncate) {
        FieldConfig fieldConfig = field.getRelevantConfig(issue);
        String mlcsFieldId = MdsfDAO.getMlcsField(fieldConfig);
        if (mlcsFieldId == null || mlcsFieldId.equals("")) {
            // TODO is logging excessive here?
            return null;
        }

        List<String> mlcsValue = getMlcsSpecificValue(issue, fieldConfig, mlcsFieldId);
        if (mlcsValue.size() == 0) {
            log.debug("No value was found in MLCS field " + mlcsFieldId + " for issue " + issue.getKey());
            return null;
        }

        log.debug("Looking for a combination based on MLCS value: " + mlcsValue);
        mlcsValue = MdsfDAO.adjustMlcsValue(mlcsValue, fieldConfig, truncate);
        if (mlcsValue == null) {
            log.debug("Unable to adjust MLCS value for issue " + issue.getKey());
            return null;
        }

        // The mlcsValue should have already been imported into the MDSF field and the dependent fields
        Map<String, String> data = MdsfDAO.convertMlcsValueToMap(mlcsValue, fieldConfig);
        if (data == null) {
            log.warn("Unable to convert the MLCS value: " + mlcsValue);
            return null;
        }
        String value = MdsfDAO.convertMapToOptionString(data);
        final Options options = optionsManager.getOptions(fieldConfig);
        Option comboOption = options.getOptionForValue(value, null);
        if (comboOption == null) {
            log.warn("Unable to find the expected option for MLCS value: " + value);
            return null;
        }

        // Set the value in the current issue
        Collection coll = new ArrayList();
        coll.add(comboOption.getValue());
        customFieldValuePersister.updateValues(field, issue.getId(), getDatabaseType(), coll, null);

        // Create the object to be returned
        Combo combo = new Combo(data);
        combo.setCurrentfields(MdsfDAO.getCurrentFields(fieldConfig));

        // Clear the old values so that if MDSF is cleared these values are not used again
        MdsfDAO.removeMlcsValues(issue.getId(), new Long(mlcsFieldId));

        return combo;
    }

    /**
     * @return the MLCS field value for a specific issue as an array
     * of the original Option values. If no value was present, then an
     * empty list is returned. The length of the list is not fixed.
     */
    private List<String> getMlcsSpecificValue(Issue issue, FieldConfig fieldConfig, String mlcsFieldId) {
        log.debug("Looking for data in the connected MLCS field customfield_" + mlcsFieldId);
        mlcsFieldId = MdsfDAO.getMlcsFieldId(mlcsFieldId);

        List mlcsValues = MdsfDAO.getMlcsValues(issue.getId(), new Long(mlcsFieldId));
        Map<String, String> optionValues = new HashMap<String, String>();
        for (Iterator it = mlcsValues.iterator(); it.hasNext(); ) {
            GenericValue gv = (GenericValue)it.next();
            String stringvalue = gv.getString("stringvalue");
            String parentkey = gv.getString("parentkey");
            if (parentkey == null) {
                parentkey = "-1";
            }
            optionValues.put(parentkey, stringvalue);
        }

        List<String> result = new ArrayList();
        String currentKey = "-1"; // The first value in a cascading select
        while (true) {
            if (!optionValues.containsKey(currentKey)) {
                break;
            }
            String optionId = optionValues.get(currentKey);
            if (optionId == null) {
                log.warn("Unable to extract a complete chain of values from MLCS field data: " + optionValues);
                return result;
            }
            String optionValue = MdsfDAO.getOptionValue(optionId, fieldConfig);
            result.add(optionValue);
            currentKey = optionId;
        }
        return result;
    }

    /**
     * This method is the same as the one in AbstractSingleFieldType
     * except that the call to getObjectFromDbValue passes in a
     * FieldConfig object.
     * This method is called by CustomFieldImpl updateIssue() when an
     * issue is updated.
     *
     * @return a Combo object, or null for all None
     */
    public Object getValueFromCustomFieldParams(CustomFieldParams relevantParams) throws FieldValidationException
    {

        log.debug("Entered getValueFromCustomFieldParams with " + removeAdditionalValidationParameters(relevantParams.getKeysAndValues()));

        if (relevantParams == null) {
            return null;
        }

        Map<String, String> pMap = convertCFParamsToMap(relevantParams);

        boolean isAllNone = allNone(pMap);
        if (isAllNone) {
            log.debug("The chosen combo was all None");
            return null;
        }

        String singleParam = MdsfDAO.convertMapToOptionString(pMap);
        if (TextUtils.stringSet(singleParam)) { //only validate if the value is set
            final FieldConfigManager fieldConfigManager = (FieldConfigManager)ComponentManager.getComponentInstanceOfType(FieldConfigManager.class);
            Long fieldConfigId = getFieldConfigParam(relevantParams);
            FieldConfig fieldConfig = fieldConfigManager.getFieldConfig(fieldConfigId);
            return getSingularObjectFromString(singleParam, fieldConfig);
        } else {
            return null;
        }
    }

    /**
     * Extract the field config id from the parameters passed from an
     * edit screen
     *
     * @return the id or null if not found
     */
    private Long getFieldConfigParam(CustomFieldParams relevantParams) {
        @SuppressWarnings("unchecked")

        final Collection<String> params = removeAdditionalValidationParameters(relevantParams.getKeysAndValues());

        if ((params == null) || params.isEmpty())
        {
            return null;
        }

        String match = "fieldconfig_";
        for (final String paramValue : params) {
            if (paramValue.startsWith(match)) {
                String fieldConfigId = paramValue.substring(match.length());
                return new Long(fieldConfigId);
            }
        }
        return null;
    }

    /**
     * Extract the custom field ids and their options from the
     * parameters passed from an edit or search screen
     *
     * @return the id or null if not found
     */
    private Map<String, String> convertCFParamsToMap(CustomFieldParams relevantParams) {
        Map<String, String> pMap = new TreeMap<String, String>();

        @SuppressWarnings("unchecked")

        final Collection<String> params = removeAdditionalValidationParameters(relevantParams.getKeysAndValues());

        if ((params == null) || params.isEmpty())
        {
            return pMap;
        }

        // The values look like "customfield_NNNN_PPPPP" and
        // fieldconfig_DDDD where customfield_NNNN is the dependent
        // custom field id, PPPPP is the option id, and DDDD is the
        // field config id. PPPPP may be -1
        for (final String paramValue : params) {
            if (paramValue.startsWith("fieldconfig_")) {
                continue;
            }
            String[] parts = paramValue.split("_");

            if (parts.length != 3) {
                log.warn("Failed to parse the MDSF parameter:" + paramValue);
                continue;
            }
            pMap.put(parts[0] + "_" + parts[1], parts[2]);
        }
        return pMap;
    }

    /**
     * @return true if all the options are None (-1) or there are no options
     */
    private boolean allNone(Map<String, String> pMap) {
        if (pMap.isEmpty()) {
            return true;
        }
        for (String s: pMap.values()) {
            if (!s.equals("-1")) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check that the strings passed in form a valid combo id, or if
     * they are all none that the field is not required.
     *
     * Called from CustomFieldImpl validateFromActionParams() and also
     * from needsMove() to check that the value is valid in another
     * context.
     * CustomFieldImpl validateParams() checks for required fields and is
     * only called if this method has no errors.
     * Checking for a present value is done with getValueFromCustomFieldParams != null
     *
     * @param config the target field config for a move
     */
    @Override
    public void validateFromParams(final CustomFieldParams relevantParams,
                                   final ErrorCollection errorCollectionToAddTo,
                                   final FieldConfig config) {
        // This should contain an MDSF option id in separate parts
        log.debug("Entered validateFromParams with " + relevantParams.getKeysAndValues());

        // have to see if it is valid in the config
        @SuppressWarnings("unchecked")

        final Collection<String> params = removeAdditionalValidationParameters(relevantParams.getKeysAndValues());

        if (params != null && !params.isEmpty()) {
            String param = params.iterator().next();
            if (param.indexOf("customfield_") == -1 && param.indexOf("fieldconfig_") == -1 && param.indexOf("-1") == -1) {
                log.debug("Checking a value for a move: " + param);

                String msg = "MDSF fields always need to be mapped in a move";
                errorCollectionToAddTo.addError(config.getCustomField().getId(), msg); // TODO i18n
                return;
            }
        }

        Map<String, String> pMap = convertCFParamsToMap(relevantParams);
        log.debug("GOT map " + pMap);

        boolean isAllNone = allNone(pMap);
        if (isAllNone) {
            log.debug("The chosen combo was all None");
            return;
        }

        final String selectedString = MdsfDAO.convertMapToOptionString(pMap);

        // TODO the problem here is that the value passed from the search
        // HTML is an option id not an option string and there is only
        // one instance of the customfield_NNNNN key. But that could be an MDSF field with just one level.
        // Should probably change the HTML to be the multivalue but how to do that in a select?
        log.debug("GOT selectedString " + selectedString);
    }

    /**
     * Return a string that displays possible options in an error message.
     */
    private String createValidOptionsString(Collection<Combo> combos) {
        final StringBuffer sb = new StringBuffer();
        for (Combo combo : combos) {
            sb.append(combo.toString());
            sb.append(" or ");
        }
        return sb.toString();
    }

    // Methods for the DEFAULT Configuration Item type
    public Object getDefaultValue(FieldConfig fieldConfig) {
        try {
            Object databaseValue = genericConfigManager.retrieve(DEFAULT_VALUE_TYPE, fieldConfig.getId().toString());
            if (databaseValue != null) {
                return getObjectFromDbValue(databaseValue, fieldConfig);
            }
            else {
                return null;
            }
        }
        catch (FieldValidationException e) {
            log.error("Incorrect formatted custom field stored as default", e);
            return null;
        }
    }

    public void removeValue(final CustomField field, final Issue issue, final Option option) {
        customFieldValuePersister.removeValue(field, issue.getId(), getDatabaseType(), option.getValue());
    }

    /**
     * Return all issues that have the given Option's value set in some field.
     * Used when a Combo is removed.
     */
    public Set getIssueIdsWithValue(final CustomField field, final Option option) {
        if (option != null) {
            return customFieldValuePersister.getIssueIdsWithValue(field, PersistenceFieldType.TYPE_LIMITED_TEXT, option.getValue());
        } else {
            return Collections.EMPTY_SET;
        }
    }

    public Options getOptions(final FieldConfig config, final JiraContextNode jiraContextNode) {
        return optionsManager.getOptions(config);
    }

    public ProjectCustomFieldImporter getProjectImporter() {
        return projectCustomFieldImporter;
    }

    public int compare(final Combo customFieldObjectValue1, final Combo customFieldObjectValue2, final FieldConfig fieldConfig) {
        return customFieldObjectValue1.compareTo(customFieldObjectValue2);
    }

    /**
     * This method is to check if the current customfield have been configure in the current project
     * Without checking the method will hit into null pointer exception because of the undefined configuration
     * @param field custom field
     * @param issue current issue which is been creating or editing
     * @return If the field is configured in the project will return true, otherwise will return false
     */
    public boolean isFieldConfiged(CustomField field, Issue issue) {
    	if(field.getRelevantConfig(issue) == null) {
    		return false;
    	}
    	return true;
    }

    /**
     * Update the select field associated with the MDSF customfield.
     * For example: If there is field A, B and C is using as combination in MDSF field.
     * No matter Field A, B or C have changed value in MDSF field,
     * the individual field A, B and C should update with the same value as well.
     *
     * @see <a href="https://tools.servicerocket.com/browse/AN111-45">AN111-45</a>
     *
     * @param issue current issue which is been creating or editing
     * @param customField select list customfield which associate in MDSF customfield
     * @param optionValue the value been chosen in MDSF
     */
    public void updateSelectField(final Issue issue, CustomField customField, String optionValue) {

        if(customField.isInScope(issue.getProjectObject(), Arrays.asList(issue.getIssueTypeObject().getId()))) {
            Options options = optionsManager.getOptions(customField.getRelevantConfig(new IssueContextImpl(issue.getProjectObject(), issue.getIssueTypeObject())));
            Option option = options.getOptionForValue(optionValue, null);

            ((MutableIssue)issue).setCustomFieldValue(customField, option);
        } else {
            log.debug(format("Issue %s is not associate with %s", issue.getId(), customField.getName()));
        }
    }
}
