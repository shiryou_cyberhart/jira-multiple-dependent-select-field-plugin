package net.customware.jira.plugins.mdsf;

import com.atlassian.jira.issue.customfields.option.Option;

/**
 * A wrapper class to help with supporting -1 (None) as an option.
 */
public class MdsfOption {

    private Long optionid;
    private String value;

    public MdsfOption(Option option) {
        this.optionid = option.getOptionId();
        this.value = option.getValue();
    }

    public MdsfOption(Long optionid, String value) {
        this.optionid = optionid;
        this.value = value;
    }

    public Long getOptionId() {
        return optionid;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return optionid + ":" + value;
    }
}
