package net.customware.jira.plugins.mdsf;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.ofbiz.DefaultOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;

import java.util.*;

// Static data retrieval methods
public class MdsfDAO {
    // This class is what MdfsCFType uses for logging
    public static final Logger log = Logger.getLogger(AbstractSingleFieldType.class);

    private final static int SEQ_OFFSET = 2;

    public static final String FIELD_SEP = " "; // avoid regex

    private static PropertySet ofbizPs = null;

    /*
     * This was selected by a process of pidooma.
     * The next id is stored in OSPropertyEntry in SEQUENCE_VALUE_ITEM
     * TODO request a value properly
     */
    private static final int MDSF_ENTITY_ID = 15005;
    private static final String PROP_POP_TO_DEPENDENT_FIELD = "mdsf.associate.select.field";

    private static PropertySet getPS() {
        if (ofbizPs == null) {
            HashMap ofbizArgs = new HashMap();
            ofbizArgs.put("delegator.name", "default");
            ofbizArgs.put("entityName", "mdsf_fields");
            ofbizArgs.put("entityId", new Long(MDSF_ENTITY_ID));
            ofbizPs = PropertySetManager.getInstance("ofbiz", ofbizArgs);
        }
        return ofbizPs;
    }

    /**
     *
     */
    public static String getCurrentFieldText(FieldConfig fieldConfig) {
        // mysql> select * from propertyentry where entity_name='mdsf_fields';
        // propertyentry.id is the foreign key into propertystring table
        // mysql> select * from propertystring where id=10341;
        String entityName = getEntityName(fieldConfig);
        return getPS().getString(entityName);
    }

    /**
     * @param value is the select custom field ids in order
     */
    public static void setCurrentFieldText(FieldConfig fieldConfig, String value) {
        String entityName = getEntityName(fieldConfig);
        log.debug("Setting property string: " + entityName + "=" + value);
        getPS().setString(entityName, value);
    }

    public static Boolean getPopulateToDependentField(FieldConfig fieldConfig){
        String entityName = PROP_POP_TO_DEPENDENT_FIELD + getEntityName(fieldConfig);
        return Boolean.valueOf(getPS().getString(entityName));
    }

    public static void setPopulateToDependentField(FieldConfig fieldConfig, Boolean value){
        String entityName = PROP_POP_TO_DEPENDENT_FIELD + getEntityName(fieldConfig);
        getPS().setString(entityName, value.toString());
    }

    /**
     * @return the propertyentry.property_key value used to identify
     * an entry in propertystring which is a Combo value as a string.
     * The Combo is a per-field and per-config instance.
     */
    private static String getEntityName(FieldConfig fieldConfig) {
        String psEntityName = fieldConfig.getCustomField().getId() + "_" + fieldConfig.getId() + "_fields";
        return psEntityName;
    }

    /**
     * @return a string of format "fieldid_contextid" where contextid
     * may be "all"
     */
    public static String getMlcsField(FieldConfig fieldConfig) {
        // MDSF 4.3.2 added support for contexts in MLCS fields
        String entityName = getMlcsEntityName(fieldConfig);
        return getPS().getString(entityName);
    }

    /**
     * @return the first part of the field id, less the context id
     */
    public static String getMlcsFieldId(String mlcsField) {
        if (mlcsField == null) {
            return mlcsField;
        }
        int idx = mlcsField.indexOf("_");
        if (idx == -1) {
            return mlcsField;
        }
        return mlcsField.substring(0, idx);
    }

    public static void setMlcsField(FieldConfig fieldConfig, String value) {
        String entityName = getMlcsEntityName(fieldConfig);
        log.debug("Setting property string: " + entityName + "=" + value);
        getPS().setString(entityName, value);
    }

    protected static void clearMlcsField(FieldConfig fieldConfig) {
        String entityName = getMlcsEntityName(fieldConfig);
        log.debug("Clearing property string: " + entityName);
        // Can't clear what isn't there
        if (getPS().getString(entityName) != null) {
            getPS().remove(entityName);
        }
    }

    private static String getMlcsEntityName(FieldConfig fieldConfig) {
        String psEntityName = fieldConfig.getCustomField().getId() + "_" + fieldConfig.getId() + "_mlcs";
        return psEntityName;
    }

    // This is all necessary because the MLCS field is not recognized as a
    // valid custom field in JIRA 4.0
    // TODO could it be without all the other functionality?
    protected static final String ENTITY_ISSUE_ID = "issue";
    protected static final String ENTITY_ID = "id";
    protected static final String ENTITY_CUSTOMFIELDTYPEKEY = "customfieldtypekey";
    protected static final String ENTITY_CUSTOMFIELD_ID = "customfield";
    protected static final String ENTITY_CUSTOMFIELD_CONFIG_ID = "customfieldconfig";
    protected static final String ENTITY_FIELD_ID = "fieldid";

    protected static final String TABLE_CUSTOMFIELD = "CustomField";
    protected static final String TABLE_CUSTOMFIELD_VALUE = "CustomFieldValue";
    protected static final String TABLE_CUSTOMFIELD_OPTION = "CustomFieldOption";
    protected static final String TABLE_FIELD_CONFIG_SCHEME = "FieldConfigScheme";

    /**
     * The cached set of rows from CustomField that have the MLCS key
     */
    protected static List<GenericValue> mlcsFields;

    /**
     * @return List containing the information about all MLCS fields present
     */
    public static List<GenericValue> getMlcsFields() {
        final Map limitClause = EasyMap.build(ENTITY_CUSTOMFIELDTYPEKEY, "com.sourcesense.jira.plugin.cascadingselect:multi-level-cascading-select");
        OfBizDelegator delegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        mlcsFields = delegator.findByAnd(TABLE_CUSTOMFIELD, limitClause);
        return mlcsFields;
    }

    /**
     * @return List containing the information about all contexts for the given MLCS fields
     */
    public static List<GenericValue> getMlcsFieldContexts(GenericValue mlcsField) {
        String key = "customfield_" + mlcsField.getString("id").toString();
        final Map limitClause = EasyMap.build(ENTITY_FIELD_ID, key);
        OfBizDelegator delegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        List<GenericValue> mlcsFieldContexts = delegator.findByAnd(TABLE_FIELD_CONFIG_SCHEME, limitClause);
        return mlcsFieldContexts;
    }

    /**
     * @param id the custom field id "customfield_NNNNN" of an MLCS field
     * @return the name of the custom field, e.g. "My Field Name"
     */
    public static String getMlcsFieldName(String id) {
        if (mlcsFields == null) {
            getMlcsFields();
        }
        for (GenericValue mlcsField: mlcsFields) {
            String key = "customfield_" + mlcsField.getString("id").toString();
            String value = mlcsField.getString("name");
            if (key.equals(id)) {
                return value;
            }
        }
        return "";
    }

    /**
     * @return List of GenericValue objects containing the rows from
     * customfieldvalue, the values of the MLCS field for an issue
     * TODO this use of the database needs to be checked if the schema changes
     */
    public static List getMlcsValues(Long issueId, Long customFieldId) {
        final Map limitClause = EasyMap.build(ENTITY_ISSUE_ID, issueId,
                                              ENTITY_CUSTOMFIELD_ID, customFieldId);
        OfBizDelegator delegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        return delegator.findByAnd(TABLE_CUSTOMFIELD_VALUE, limitClause);
    }

    public static List getAllMlcsOptions(Long customFieldId, String customFieldContextId) {
        final Map limitClause = EasyMap.build(ENTITY_CUSTOMFIELD_ID, customFieldId);
        if (!customFieldContextId.equals("all")) {
            limitClause.put(ENTITY_CUSTOMFIELD_CONFIG_ID, customFieldContextId);
        }
        OfBizDelegator delegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        return delegator.findByAnd(TABLE_CUSTOMFIELD_OPTION, limitClause);
    }

    /**
     * @return the option value for the given MLCS option id
     * TODO this use of the database needs to be checked if the schema changes
     * TODO restrict by field id
     */
    public static String getOptionValue(String optionId, FieldConfig fieldConfig) {
        final Map limitClause = EasyMap.build(ENTITY_ID, new Long(optionId));
        OfBizDelegator delegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        List gvs = delegator.findByAnd(TABLE_CUSTOMFIELD_OPTION, limitClause);
        if (gvs.size() == 0) {
            log.warn("MLCS unknown option id " + optionId);
            return null;
        } else if (gvs.size() > 1) {
            log.warn("MLCS more than one option with id " + optionId);
            return null;
        }
        GenericValue gv = (GenericValue)gvs.get(0);
        return gv.getString("value"); // See entitymodel.xml for customvalue
    }

    /**
     * TODO add getDatabaseType and getColumnName(persistenceFieldType) to the EasyMap?
     */
    public static void removeMlcsValues(Long issueId, Long customFieldId) {
        final Map limitClause = EasyMap.build(ENTITY_ISSUE_ID, issueId,
                                              ENTITY_CUSTOMFIELD_ID, customFieldId);
        OfBizDelegator delegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        List<GenericValue> gvs = delegator.findByAnd(TABLE_CUSTOMFIELD_VALUE, limitClause);
        delegator.removeAll(gvs);
    }

    /**
     * Return the fields that are being used as dependent fields for a
     * MDSF field, in the same order as defined in the PropertySet.
     *
     * @return a list of custom fields in the current display order
     */
    public static List<CustomField> getCurrentFields(FieldConfig fieldConfig) {
        // TODO could cache the fields based on fieldConfig until the order changes

        //log.debug("Entering getCurrentFields");
        ArrayList<CustomField> result = new ArrayList<CustomField>();
        String entityName = getEntityName(fieldConfig);
        String currentFieldText = getPS().getString(entityName);
        //log.debug("Retrieved currentFieldText=" + currentFieldText);

        if (currentFieldText == null || currentFieldText.equals("")) {
            return result; // No dependent fields have been configured yet
        }
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        String[] currentFieldIds = currentFieldText.split(FIELD_SEP);
        for (String fieldId: currentFieldIds) {
        	CustomField cf = customFieldManager.getCustomFieldObject(fieldId);
            if (cf == null) {
                log.warn("getCurrentFields: custom field " + fieldId + " does not exist");
                // TODO remove from the currentFieldText here and resave it?
                continue;
            }
            result.add(cf);
        }
        return result;
    }

    /**
     * @return the string representation of an all Nones combo for javascript
     */
    @HtmlSafe
    public String getAllNonesJS(List<CustomField> currentFields) {
        if (currentFields == null) {
            // log.warn("No fields found for JS representation of all None combo");
            return "[]";
        }

        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i=0; i<currentFields.size(); i++) {
            CustomField cf = currentFields.get(i);
            String otherid = cf.getId() + "_" + "-1";
            String text = "None"; // TODO i18n
            sb.append("\n [\"" + otherid + "\", \"" + text + "\"]");
            if (i < currentFields.size() - 1) {
                sb.append(",");
            }
         }
        sb.append("\n]");
        return sb.toString();
    }

    /**
     * Return the currently defined combinations for an MDSF field in
     * the order specified by the sequence numbers in the dependent fields.
     *
     * @param fieldConfig
     *
     * @return the currently defined combinations for the MDSF field in
     * the order specified by the sequence numbers in the dependent fields.
     * If no combos are configured yet, then return an empty Collection.
     */
    public static Collection<Combo> getCurrentCombos(FieldConfig fieldConfig)
    {
        // TODO could cache the results based on fieldConfig until a
        // combo is added or removed or the sequence order of the
        // underlying options changes or the current fields change.

        Map<Long, Combo> ordered = new TreeMap<Long, Combo>();
        List<CustomField> currentFields = getCurrentFields(fieldConfig);
        // The valid combinations are stored as options for this field
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        // The combos are stored as regular options of an MDSF field
        // and look like: customfield_NNNN MMMMM customfield_OOOO PPPP
        Options options = optionsManager.getOptions(fieldConfig);
        for (Iterator it = options.iterator(); it.hasNext(); ) {
            Option option = (Option)it.next();
            String optionText = option.getValue();
            Map<String, String> valuesMap = convertOptionStringToMap(optionText);
            if (valuesMap == null) {
                continue; // errors already logged
            }
            Combo combo = new Combo(valuesMap);
            // Add this to make the Combo displayable with getRow() or
            // toString()
            combo.setCurrentfields(currentFields);
            // Add this to make it easier to refer to a particular
            // combo in a screen
            combo.setOptionid(option.getOptionId());

            Long overallSeq = getComboSequence(combo, currentFields, valuesMap, optionsManager);
            // Don't overwrite existing entries. The sequence number
            // may change between calls to this method but that's ok.
            while (ordered.containsKey(overallSeq)) {
                overallSeq++;
            }
            ordered.put(overallSeq, combo);
        }
        // The values are nicely ordered by the overallSeq value
        return ordered.values();
    }

    /**
     * If the field ids and values in the constraints match the ones
     * in the Combo, ignoring ones that aren't referred to in the
     * constraints, then return true.
     */
    private static boolean comboMatch(Combo combo, Map<String, String> constraints) {
        for (Iterator it = constraints.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry)it.next();
            String constraintFieldId = (String)entry.getKey();
            String constraintOptionId = (String)entry.getValue();
            String comboOptionId = combo.getRawOptionId(constraintFieldId);
            if (comboOptionId == null) {
                log.warn("Combo does not have a value for constraint field " + constraintFieldId + ", combo=" +  combo.getMap());
                return false;
            }
            if (!comboOptionId.equals(constraintOptionId)) {
                log.debug("Combo does not match constraint field " + constraintOptionId + ", combo=" +  combo.getMap());
                return false;
            }
        }
        log.debug("Combo: " + combo.getMap() + " matches constraints: " + constraints);
        return true;
    }

    /**
     * @return all the options for the given custom field in sequence
     * order. If no options exist, return null. The None option is not
     * included here.
     */
    public static Options getFieldOptions(String fieldId)
    {
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        CustomField cf = customFieldManager.getCustomFieldObject(fieldId);
        if (cf == null) {
            log.warn("getFieldOptions: custom field " + fieldId + " does not exist");
            // TODO remove from the currentFieldText?
            return null;
        }
        // Always use the global issue context
        IssueContext issueContext = new IssueContextImpl((Long)null, (String)null);
        // These are in sequence order
        return cf.getOptions(null, cf.getRelevantConfig(issueContext), null);
    }

    /**
     * @param fieldId the MDSF custom field
     * @param combos the valid combinations for the MDSF custom field,
     * maybe emtpty
     * @return the constrained options for the given custom field.
     * These are the Options for the custom field that appear in the
     * given combos in the same order that they came from the
     * OptionManager, not the order that they appear in the combos.
     * This is so that the order of options in an individual field doesn't
     * change for the user.
     * They also include the all None combo to clear the field.
     */
    public static Collection<MdsfOption> getConstrainedFieldOptions(String fieldId, Collection<Combo> combos) {
    	Map<Long, MdsfOption> orderedSeq = new TreeMap<Long, MdsfOption>();

        // None comes first and is an option for every dependent field
        MdsfOption mdsfOption  = new MdsfOption(new Long("-1"), "None"); // TODO i18n
        orderedSeq.put(new Long(1), mdsfOption);
        Set<String> alreadySeen = new HashSet<String>();
        alreadySeen.add("-1");

        Options options = getFieldOptions(fieldId);
        if (options == null) {
            return orderedSeq.values();
        }

        // TODO this could be slow with a lot of options. Worth
        // keeping a cache around?
        Long selectedCombo = null;
        for (Combo combo: combos) {
            String comboOptionId = combo.getRawOptionId(fieldId);
            if (comboOptionId == null || alreadySeen.contains(comboOptionId)) {
                continue;
            }

            // Combos can have None (-1) as part of their definition
            if (comboOptionId.equals("-1")) {
                mdsfOption  = new MdsfOption(new Long("-1"), "None"); // TODO i18n
                orderedSeq.put(new Long(1), mdsfOption); // None comes first
                //log.debug("getConstrainedFieldOptions: field " + fieldId + " adding option None at sequence 1");
            } else {
                Long comboOptionIdL;
                try {
                    comboOptionIdL = new Long(comboOptionId);
                } catch (NumberFormatException nfe) {
                    log.warn("Expected an integer, got " + comboOptionId);
                    continue;
                }
                Option option = options.getOptionById(comboOptionIdL);
                if (option == null) {
                    log.error("Unable to find an option for field " + fieldId + " with id " + comboOptionId);
                    continue;
                }
                Long newSeq = option.getSequence() + SEQ_OFFSET;
                //log.debug("getConstrainedFieldOptions: field " + fieldId + " adding option " + option.getValue() + " at sequence " + newSeq);
                mdsfOption  = new MdsfOption(option);
                // Sequence numbers are unique here
                orderedSeq.put(newSeq, mdsfOption);
            }
            alreadySeen.add(comboOptionId);
        }

        return orderedSeq.values();
    }

    /**
     * Convert the combo data to an option value. The option value should be
     * unchanged by the order of the fields.
     */
    public static String convertMapToOptionString(Map map) {
        StringBuffer sb = new StringBuffer();
        for (Iterator it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry)it.next();
            String fieldid = (String)entry.getKey();
            String optionid = (String)entry.getValue();
            sb.append(fieldid);
            sb.append(MdsfDAO.FIELD_SEP);
            sb.append(optionid);
            sb.append(MdsfDAO.FIELD_SEP);
        }
        return sb.toString().trim();
    }

    /**
     * Parse an MDSF option string and return a map with the sorted
     * field ids and options ids. The optionText string looks like:
     * customfield_10000 10002 customfield_10001 10004 customfield_10002 10011
     * Note that customfield 10000 is before 10001 but more
     * importantly the order is consistent; ascii or numeric order
     * doesn't matter.
     */
    public static Map<String, String> convertOptionStringToMap(String optionText) {
        log.debug("Parsing MDSF option: " + optionText);
        if (optionText == null || optionText.trim().equals("")) {
            // This method shouldn't be called with an empty string
            // but check for it anyway
            log.debug("Unable to parse an empty value for MDSF option");
            return null;
        }

        String[] values = optionText.split(FIELD_SEP);
        if (values.length % 2 != 0) {
            log.error("Unable to parse the MDSF option value as a valid combination: " + optionText);
            return null;
        }

        TreeMap<String, String> valuesMap = new TreeMap<String, String>();
        for (int i=0; i< values.length; i++) {
            // TODO check this is still a valid custom field
            String dependentFieldId = values[i];
            // TODO check this is still a valid option for the previous custom field or -1 for None
            String dependentFieldOptionId = values[i+1];
            valuesMap.put(dependentFieldId, dependentFieldOptionId);
            i++;
        }
        // TODO MDSF-11 add -1 for newly added fields
        return valuesMap;
    }

    /**
     * Get a unique sequence number for a given Combo, made up of the
     * sequence numbers of the underlying custom fields in their
     * current order. The exact value may change between calls.
     */
    private static Long getComboSequence(Combo combo, List<CustomField> currentFields, Map<String, String> valuesMap, OptionsManager optionsManager) {
        Long[] seqs = new Long[currentFields.size()];
        for (int i=0; i < currentFields.size(); i++) {
            CustomField cf = currentFields.get(i);
            String optStr = (String)valuesMap.get(cf.getId());
            if (optStr == null) {
                // There is no entry recorded for the current field in the
                // combo data
                seqs[i] = new Long(0);
            } else if (optStr.equals("-1")) {
                seqs[i] = new Long(1); // None always comes first
            } else {
                Long optId = new Long(optStr);
                Option opt = optionsManager.findByOptionId(optId);
                if (opt == null) {
                    log.warn("Option " + optId + " no longer exists in custom field " + cf.getName());
                    // TODO remove it elsewhere?
                    seqs[i] = new Long(0);
                } else {
                    // Allow for not found and none values, so start at 2
                    seqs[i] = opt.getSequence() + SEQ_OFFSET;
                }
            }
        }
        // Now create a unique and stable integer for sorting on.
        // Sequence numbers are always positive.
        Long overallSeq = new Long(0);
        for (int i=0; i<seqs.length; i++) {
            // TODO assumes < 1K options per dependent field
            overallSeq = (overallSeq * 1000) + seqs[i];
        }
        return overallSeq;
    }

    public static String getBaseUrl() {
        // TODO how to get the sal applicationProperties statically?
        // import com.atlassian.sal.api.ApplicationProperties;
        // applicationProperties.getBaseUrl();
        return ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
    }

    public static final String NONE_LABEL = "EmptyMlcsValue_uid";

    /**
     * Change the given MLCS value to be the right size for the MDSF field.
     */
    public static List<String> adjustMlcsValue(List<String> mlcsValue,
                                               FieldConfig fieldConfig,
                                               boolean truncate) {
        List<CustomField> currentFields = getCurrentFields(fieldConfig);
        if (currentFields.size() < mlcsValue.size()) {
            if (!truncate) {
                String msg = "The number of select levels differ. The MLCS field has " + mlcsValue.size() + " and the MDSF field has " + currentFields.size();
                log.warn(msg);
                return null;
            } else {
                // Already shown in Path: but useful anyway
                StringBuffer mlcsSb = new StringBuffer();
                for (String mlcsSubvalue : mlcsValue) {
                    mlcsSb.append(mlcsSubvalue);
                    mlcsSb.append(",");
                }
                String msg = "Creating a truncated MDSF combo from " + mlcsSb.toString() + " because the number of select levels differ. The MLCS field has " + mlcsValue.size() + ", and the MDSF field has " + currentFields.size();
                log.warn(msg);
                List<String> tmpList = new ArrayList<String>();
                int fieldIdx = 0;
                while (fieldIdx<currentFields.size()) {
                    tmpList.add(mlcsValue.get(fieldIdx));
                    fieldIdx++;
                }
                while (fieldIdx<mlcsValue.size()) {
                    log.warn("Discarded MLCS option subvalue: " + mlcsValue.get(fieldIdx));
                    fieldIdx++;
                }
                mlcsValue = tmpList;
            }
        }

        // Add non-existent option values as None options
        int numNone = currentFields.size() - mlcsValue.size();
        if (numNone > 0) {
            log.debug("Adding " + numNone + " None value(s) to " + mlcsValue);
            for (int i=0; i<numNone; i++) {
                mlcsValue.add(NONE_LABEL);
            }
        }

        return mlcsValue;
    }

    public static Map<String, String> convertMlcsValueToMap(List<String> mlcsValue, FieldConfig fieldConfig) {
        List<CustomField> currentFields = getCurrentFields(fieldConfig);
        // Remember that the fields are always sorted in an MDSF option string
        TreeMap<String, String> data = new TreeMap<String, String>();
        for (int i=0; i<mlcsValue.size(); i++) {
            String mlcsSubValue = (String)mlcsValue.get(i);
            CustomField cf = (CustomField)currentFields.get(i);
            String optionId;
            if (mlcsSubValue.equals(MdsfDAO.NONE_LABEL)) {
                optionId = "-1";
            } else {
                Options options = MdsfDAO.getFieldOptions(cf.getId());
                Option option = options.getOptionForValue(mlcsSubValue, null);
                if (option == null) {
                    log.warn("Unable to find an option " + mlcsSubValue + " in the custom field '" + cf.getName()  + "' (" + cf.getId() + ")");
                    return null;
                }
                optionId = option.getOptionId().toString();
            }
            data.put(cf.getId(), optionId);
        }
        return data;
    }

    public static String convertMlcsValueToOptionString(List<String> mlcsValue, FieldConfig fieldConfig) {
        Map<String, String> data = convertMlcsValueToMap(mlcsValue, fieldConfig);
        if (data == null) {
            return null;
        }
        return convertMapToOptionString(data);
    }
}


