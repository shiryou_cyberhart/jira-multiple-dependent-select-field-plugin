package net.customware.jira.plugins.mdsf;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.MultipleSettableCustomFieldType;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.OptionUtils;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.web.action.admin.customfields.AbstractEditConfigurationItemAction;
import com.atlassian.sal.api.ApplicationProperties;
import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import java.util.*;

/**
 * This Config Item is like SettableOptionsConfigItem but the options are
 * the names of the available custom fields.
 *
 */

public class MdsfConfigItem implements FieldConfigItemType {
    private final CustomFieldType customFieldType;
    private final OptionsManager optionsManager;
    private final CustomFieldManager customFieldManager;
    private final ApplicationProperties applicationProperties;

    public MdsfConfigItem(CustomFieldType customFieldType, 
                          final OptionsManager optionsManager,
                          final CustomFieldManager customFieldManager,
                          final ApplicationProperties applicationProperties)
    {
        this.customFieldType = customFieldType;
        this.optionsManager = optionsManager;
        this.customFieldManager = customFieldManager;
        this.applicationProperties = applicationProperties;
    }

    public String getDisplayName()
    {
        return "Dependent Fields";
    }

    public String getDisplayNameKey()
    {
        return "MDSF Fields and Combinations"; // TODO i18n
    }

    public String getViewHtml(FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem) {
        List<CustomField> currentFields = MdsfDAO.getCurrentFields(fieldConfig);
        final StringBuffer sb = new StringBuffer();
        if (currentFields.isEmpty()) {
            sb.append("No fields configured."); // TODO i18n
            return sb.toString();
        }

        sb.append("Fields:&nbsp;"); // TODO i18n
        for (Iterator it = currentFields.iterator(); it.hasNext(); ) {
            // Example URL: http://localhost:8080/secure/admin/ConfigureCustomField!default.jspa?customFieldId=10000
            CustomField cf = (CustomField)it.next();
            sb.append("<a href=\"");
            sb.append(applicationProperties.getBaseUrl());
            sb.append("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=");
            sb.append(cf.getIdAsLong());
            sb.append("\">");
            sb.append(cf.getName());
            sb.append("</a>");
            
            if (it.hasNext()) {
                sb.append(",&nbsp;");
            }
        }

        // Also display a read-only version of the current combos
        Collection<Combo> combos = MdsfDAO.getCurrentCombos(fieldConfig);
        if (combos.isEmpty()) {
            sb.append("<br/>");
            sb.append("No combinations defined."); // TODO i18n

            sb.append(getDisplayMlcsField(fieldConfig));

            return sb.toString();    
        }
        
        sb.append("<p>");
        sb.append("<br/>");
        sb.append("Combinations:");
        sb.append("</p>");
        sb.append("<ul>");
        for (Combo combo: combos) {
            sb.append("<li>");
            sb.append(combo.toString());
            sb.append("</li>");
        }
        sb.append("</ul>");
    
        sb.append(getDisplayMlcsField(fieldConfig));

        return sb.toString();    
    }
    
    private String getDisplayMlcsField(FieldConfig fieldConfig) {
        StringBuffer sb = new StringBuffer();
        String mlcsField = MdsfDAO.getMlcsField(fieldConfig);
        if (mlcsField != null && !mlcsField.equals("")) {
            sb.append("<br/>");
            sb.append("MLCS field: ");
            String mlcsFieldId = MdsfDAO.getMlcsFieldId(mlcsField);
            String mlcsFieldName = MdsfDAO.getMlcsFieldName("customfield_" + mlcsFieldId);
            sb.append(mlcsFieldName);
            sb.append(" (customfield_");
            // TODO show the context separately
            sb.append(mlcsField);
            sb.append(")");
        }
        return sb.toString();
    }

    public String getObjectKey()
    {
        return "mdsfconfig";
    }

    /**
     * In the velocity context, there is an entry with the key that
     * was returned from getObjectKey() and containing whatever is
     * passed here.
     */
    public Object getConfigurationObject(Issue issue, FieldConfig config)
    {
        Map result = new HashMap();

        // Easier to access via the MdsfDAO object
        //result.put("combos", MdsfDAO.getCurrentCombos(config));
        //result.put("currentFields", MdsfDAO.getCurrentFields(config));

        // Everything in this is static but need a way to use it
        result.put("mdsfDAO", new MdsfDAO());
        return result;
    }

    public String getBaseEditUrl() {
        return "EditMdsf!default.jspa";
    }

}
