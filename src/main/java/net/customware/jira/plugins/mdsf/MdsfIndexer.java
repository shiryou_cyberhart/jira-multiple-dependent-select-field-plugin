package net.customware.jira.plugins.mdsf;

import static com.google.common.primitives.Ints.lexicographicalComparator;

import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.CaseFolding;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.index.indexers.impl.SelectCustomFieldIndexer;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Add index entries for the MDSF field and also for the dependent fields.
 */
public class MdsfIndexer extends SelectCustomFieldIndexer {

    public static final Logger log = Logger.getLogger(AbstractSingleFieldType.class);

    private final CustomField customField;
    private final OptionsManager optionsManager;
    private final BuildUtilsInfo buildUtils;

    public MdsfIndexer(final FieldVisibilityManager fieldVisibilityManager, 
                       final CustomField customField,
                       final OptionsManager optionsManager,
                       final BuildUtilsInfo buildUtils) {
        super(fieldVisibilityManager, customField);
        this.customField = customField;
        this.optionsManager = optionsManager;
        this.buildUtils = buildUtils;
    }

    public void addDocumentFields(final Document doc, final Issue issue, final Field.Index indexType) {
        final Object value = customField.getValue(issue);
        log.debug("Updating index for issue " + issue.getKey() + " for " + value);
        // An empty value in this field is non-null
        Combo combo = (Combo)value;
        // TODO add -1 for isAllNone
        if (combo != null && !combo.isAllNone()) {
            // Make sure what we store matches the value returned by the search vm
            if (combo.getOptionid() != null) {
                String indexValue = customField.getId() + "_" + combo.getOptionid().toString();
                doc.add(new Field(getDocumentFieldId(), indexValue, Field.Store.YES, indexType));
            } else {
                log.error("Cannot index the combo " + combo);
            }

            // Add index entries for the dependent fields here
            // Before JIRA 4.4 these were strings, but now they are options.
            // Follow SelectCustomFieldIndexer
            for (Iterator it = combo.getMap().entrySet().iterator(); it.hasNext(); ) {
                Map.Entry entry = (Map.Entry)it.next();
                try {
                    String fieldId = (String)entry.getKey();
                    String optionIdStr = (String)entry.getValue();
                    Long optLong = Long.parseLong(optionIdStr);
                    Option option = optionsManager.findByOptionId(optLong);
                    if (option != null) {
                    	String indexValue;
                    	if (lexicographicalComparator().compare(buildUtils.getVersionNumbers(), new int[] { 5, 1, 0 }) >= 0 ) {
                    		// If this is JIRA 5.1, index by ID.
                    		indexValue = optLong.toString();
                    	} else {
                    		// If this is the older JIRA, index by String.
                    		indexValue = CaseFolding.foldString(option.toString());
                    	}
                        doc.add(new Field(fieldId, indexValue, Field.Store.YES, indexType));
                    }
                } catch (NumberFormatException nfe) {
                    log.error(nfe);
                    continue;
                }
            }
        }
    }

}
