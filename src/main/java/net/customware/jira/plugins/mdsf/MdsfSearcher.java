package net.customware.jira.plugins.mdsf;

/*
  Need to offer all the values in combos plus Any and including None.
  Convert the chosen values to valid combos and search for issues with 
  any of those values in the field.
 */

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.issue.search.QueryContextConverter;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldValueProvider;
import com.atlassian.jira.issue.customfields.SingleValueCustomFieldValueProvider;
import com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.converters.SelectConverter;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.searchers.AbstractInitializationCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.CustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.SimpleCustomFieldContextValueGeneratingClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.information.CustomFieldSearcherInformation;
import com.atlassian.jira.issue.customfields.searchers.renderer.CustomFieldRenderer;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.customfields.statistics.SelectStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.issue.search.searchers.renderer.SearchRenderer;
import com.atlassian.jira.issue.search.searchers.transformer.SearchInputTransformer;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.context.MultiClauseDecoratorContextFactory;
import com.atlassian.jira.jql.context.SelectCustomFieldClauseContextFactory;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.query.ActualValueCustomFieldClauseQueryFactory;
import com.atlassian.jira.jql.util.IndexValueConverter;
import com.atlassian.jira.jql.util.JqlSelectOptionsUtil;
import com.atlassian.jira.jql.util.SimpleIndexValueConverter;
import com.atlassian.jira.jql.validator.SelectCustomFieldValidator;
import com.atlassian.jira.jql.values.CustomFieldOptionsClauseValuesGenerator;
import com.atlassian.jira.util.*;
import com.atlassian.jira.web.FieldVisibilityManager;
import net.customware.jira.plugins.mdsf.transformers.MdsfSearchInputTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Reimplemented from SelectSearcher rather than extended to
 * avoid DI pain.
 * See also http://jira.atlassian.com/browse/JRA-15623
 */
public class MdsfSearcher extends AbstractInitializationCustomFieldSearcher
        implements CustomFieldSearcher, SortableCustomFieldSearcher, CustomFieldStattable
{
    private volatile CustomFieldSearcherInformation searcherInformation;
    private volatile SearchInputTransformer searchInputTransformer;
    private volatile SearchRenderer searchRenderer;
    private volatile CustomFieldSearcherClauseHandler customFieldSearcherClauseHandler;
    private volatile ClauseNames clauseNames;

    private final OptionsManager optionsManager;
    private final ComponentFactory componentFactory;
    private final ComponentLocator componentLocator;
    private CustomFieldInputHelper customFieldInputHelper;

    public MdsfSearcher()
    {
        this.componentLocator = new JiraComponentLocator();
        this.componentFactory = JiraComponentFactory.getInstance();
        this.optionsManager = ComponentAccessor.getOptionsManager();
    }

    /**
     * This is the first time the searcher knows what its ID and names are
     *
     * @param field the Custom Field for this searcher
     */
    public void init(CustomField field)
    {
        clauseNames = field.getClauseNames();

        final FieldVisibilityManager fieldVisibilityManager = componentLocator.getComponentInstanceOfType(FieldVisibilityManager.class);
        final JqlOperandResolver jqlOperandResolver = componentLocator.getComponentInstanceOfType(JqlOperandResolver.class);
        final JqlSelectOptionsUtil jqlSelectOptionsUtil = componentLocator.getComponentInstanceOfType(JqlSelectOptionsUtil.class);
        final QueryContextConverter queryContextConverter = componentLocator.getComponentInstanceOfType(QueryContextConverter.class);
        this.customFieldInputHelper = componentLocator.getComponentInstanceOfType(CustomFieldInputHelper.class);
        final MultiClauseDecoratorContextFactory.Factory multiFactory = componentLocator.getComponentInstanceOfType(MultiClauseDecoratorContextFactory.Factory.class);
        final BuildUtilsInfo buildUtils = componentLocator.getComponentInstanceOfType(BuildUtilsInfo.class);

        final FieldIndexer indexer = new MdsfIndexer(fieldVisibilityManager, field, optionsManager, buildUtils);
        List<FieldIndexer> indexers = new ArrayList<FieldIndexer>();
        indexers.add(indexer);
        // No need to add other indexers for the dependent fields if
        // done in single custom indexer

        final IndexValueConverter indexValueConverter = new SimpleIndexValueConverter(true);

        final CustomFieldValueProvider customFieldValueProvider = new SingleValueCustomFieldValueProvider();
        this.searcherInformation = new CustomFieldSearcherInformation(field.getId(), field.getNameKey(), indexers, new AtomicReference<CustomField>(field));
        this.searchRenderer = new CustomFieldRenderer(clauseNames, getDescriptor(), field, customFieldValueProvider, fieldVisibilityManager);
        this.searchInputTransformer = new MdsfSearchInputTransformer(field, clauseNames, searcherInformation.getId(), jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper);

        this.customFieldSearcherClauseHandler = new SimpleCustomFieldContextValueGeneratingClauseHandler(
                componentFactory.createObject(SelectCustomFieldValidator.class, field),
                new ActualValueCustomFieldClauseQueryFactory(field.getId(), jqlOperandResolver, indexValueConverter, false),
                multiFactory.create(componentFactory.createObject(SelectCustomFieldClauseContextFactory.class, field), false),
                componentLocator.getComponentInstanceOfType(CustomFieldOptionsClauseValuesGenerator.class),
                OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY,
                JiraDataTypes.OPTION);
    }

    public SearcherInformation<CustomField> getSearchInformation()
    {
        if (searcherInformation == null)
        {
            throw new IllegalStateException("Attempt to retrieve SearcherInformation off uninitialised custom field searcher.");
        }
        return searcherInformation;
    }

    public SearchInputTransformer getSearchInputTransformer()
    {
        if (searchInputTransformer == null)
        {
            throw new IllegalStateException("Attempt to retrieve searchInputTransformer off uninitialised custom field searcher.");
        }
        return searchInputTransformer;
    }

    public SearchRenderer getSearchRenderer()
    {
        if (searchRenderer == null)
        {
            throw new IllegalStateException("Attempt to retrieve searchRenderer off uninitialised custom field searcher.");
        }
        return searchRenderer;
    }

    public CustomFieldSearcherClauseHandler getCustomFieldSearcherClauseHandler()
    {
        if (customFieldSearcherClauseHandler == null)
        {
            throw new IllegalStateException("Attempt to retrieve customFieldSearcherClauseHandler off uninitialised custom field searcher.");
        }
        return customFieldSearcherClauseHandler;
    }

    public StatisticsMapper getStatisticsMapper(final CustomField customField)
    {
        if (clauseNames == null)
        {
            throw new IllegalStateException("Attempt to retrieve Statistics Mapper off uninitialised custom field searcher.");
        }
        final SelectConverter selectConverter = componentLocator.getComponentInstanceOfType(SelectConverter.class);
        return new SelectStatisticsMapper(customField, selectConverter, ComponentManager.getInstance().getJiraAuthenticationContext(), customFieldInputHelper);
    }

    public LuceneFieldSorter getSorter(CustomField customField)
    {

    	return getStatisticsMapper(customField);
    }
}
