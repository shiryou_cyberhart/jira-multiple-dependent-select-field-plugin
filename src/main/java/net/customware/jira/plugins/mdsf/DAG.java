package net.customware.jira.plugins.mdsf;

import java.util.*;
import org.apache.log4j.Logger;

/**
 * A DAG has Vertex objects. Vertex objects are related by Edge objects.
 */
public class DAG {
    public static final Logger log = Logger.getLogger(DAG.class);

    public DAG() {
        this.vertices = new HashMap<String, Vertex>();
        this.edges = new HashMap<String, Edge>();
    }

    /**
     * @return an existing or newly created vertex
     */
    public Vertex addVertex(String id) {
        String vertex_key = id;
        Vertex v = vertices.get(vertex_key);
        if (v == null) {
            v = new Vertex(vertex_key);
            vertices.put(vertex_key, v);
        }
        return v;
    }

    /**
     * Only one edge between a pair of vertices is supported.
     * Cycles are not checked for.
     * Sequence valuesa are expected to be unique.
     *
     * @param label text label for an edge, may not be unique across
     * configurations
     * @param sequence the sequence in which to process edges
     * @param customfield the same label may appear in multiple MLCS fields
     * @param customfieldconfig the same customfield may have
     * different configurations
     *
     * @return an existing or newly created edge. Edges are uniquely
     * identified by their option ids.
     */
    public Edge addEdge(Vertex from, 
                        Vertex to, 
                        String label, 
                        Long sequence, 
                        Long customfield, 
                        Long customfieldconfig) {
        log.debug("Adding an edge: " + from.getId() + "->" + to.getId() + " : " + label + ", sequence " + sequence + " (customfield " + customfield + ", customfieldconfig " + customfieldconfig + ")");
        String key = from.getId() + to.getId(); // TODO should separate these?
        Edge e = edges.get(key);
        if (e == null) {
            e = new Edge(from, to, label, sequence, customfieldconfig);
            edges.put(key, e);
        }
        return e;
    }

    /**
     * Convenience method
     */
    public Edge addEdge(String from, 
                        String to, 
                        String label, 
                        Long sequence, 
                        Long customfield, 
                        Long customfieldconfig) {
        Vertex fromVertex = addVertex(from);
        Vertex toVertex = addVertex(to);
        return addEdge(fromVertex, toVertex, label, sequence, customfield, customfieldconfig);
    }


    /**
     * All paths in the DAG in the sequence order with no duplicates.
     */
    public List<Path> getPaths() {
        Map<String, Path> acc = new TreeMap<String, Path>();
        List<Vertex> roots = getRootVertices();
        for (Vertex root: roots) {
            if (!root.getId().equals("-1")) {
                log.warn("Unexpected root id: " + root.getId());
            } else {
                log.debug("Processing root id: " + root.getId());
            }
            List<Path> paths = getPaths(root);
            for (Path p: paths) {
                String key = p.toString();
                acc.put(key, p); // overwrite any existing path
            }
        }
        return new ArrayList(acc.values());
    }

    /**
     * Find all the vertices which have no edges leading into them,
     * and so are root vertices.
     */
    public List<Vertex> getRootVertices() {
        // TODO root vertex is always the one with id -1, cache it
        List<Vertex> result = new ArrayList<Vertex>();
        log.debug("Getting root vertices");
        for (Vertex v: vertices.values()) {
            boolean hasParent = false;
            for (Edge e: edges.values()) {
                if (e.getTo().equals(v)) {
                    hasParent = true;
                    break;
                }
            }
            if (!hasParent) {
                log.debug("Found root vertex " + v);
                result.add(v);
            }
        }
        return result;
    }

    /**
     * All paths from the given Vertex in the DAG, in the sequence order.
     * Also includes subpaths from the given Vertex. May contain duplicates.
     */
    public List<Path> getPaths(Vertex v) {
        log.debug("getPaths(): vertex " + v);
        List<Path> result = new ArrayList<Path>();
        // The child edges are sorted by sequence
        Set<Edge> children = getChildEdges(v);
        if (children.isEmpty()) {
            log.debug("Vertex " + v + " has no child edges");
            return result;
        } else {
            log.debug("Vertex " + v + " has " + children.size() + " child edge(s) " + children);
            
        }
        for (Edge childEdge: children) {
            log.debug("Examining child: " + childEdge);
            List<Path> paths = getPaths(childEdge.getTo());
            if (paths.isEmpty()) {
                log.debug("Child edge " + childEdge + " is last in a path");
                Path newPath = new Path();
                newPath.addEdge(childEdge);
                result.add(newPath);
            } else {
                for (Path path: paths) {
                    log.debug("Found path " + path);
                    Path newPath = new Path();
                    newPath.addEdge(childEdge);
                    newPath.addEdges(path);
                    result.add(newPath);

                    Path subPath = new Path();
                    subPath.addEdge(childEdge);
                    result.add(subPath);
                }
            }
        }
        return result;
    }

    /**
     * @return a List of Edges that have the given Vertex as a start Vertex.
     * The set is ordered according to the sequence of the edges.
     */
    public Set<Edge> getChildEdges(Vertex v) {
        Set<Edge> result = new TreeSet<Edge>(new EdgeComparator());
        for (Edge e: edges.values()) {
            if (e.getFrom().equals(v)) {
                if (result.contains(e)) {
                    log.warn("The set of child edges from vertex " + v + " already contains the edge " + e.toStringVerbose());
                }
                // Overwrite the existing one anyway
                result.add(e);
            }
        }
        return result;
    }

    private Map<String, Vertex> vertices;
    private Map<String, Edge> edges;

}

class Vertex {

    public Vertex(String id) {
        this.id = id;
    }

    /**
     * The unique identifier for each vertex, the id value from
     * customfieldoption table and -1 for the root.
     */
    private String id;

    public String getId() { 
        return id;
    }

    public boolean equals(Object o) {
        if (o instanceof Vertex) {
            Vertex other = (Vertex)o;
            if (other.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }
    
    public String toString() {
        return "" + id;
    }

}

class Edge {

    public Edge(Vertex from, 
                Vertex to, 
                String label, 
                Long sequence, 
                Long customfieldconfig) {
        this.from = from;
        this.to = to;
        this.label = label;
        this.sequence = sequence;
        this.customfieldconfig = customfieldconfig;
    }
    
    public Vertex getFrom() {
        return from;
    }

    public Vertex getTo() {
        return to;
    }

    public String getLabel() {
        return label;
    }

    public Long getSequence() {
        return sequence;
    }

    public void setCustomfieldconfig(Long value) { 
        this.customfieldconfig = value;
    }

    public Long getCustomfieldconfig() { 
        return customfieldconfig;
    }

    public String toString() {
        return from.getId() + "->" + to.getId();
    }

    public String toStringVerbose() {
        return from.getId() + "->" + to.getId() + ", \"" + label + "\"" + ", " + sequence;
    }

    public boolean equals(Object o) {
        if (o instanceof Edge) {
            Edge other = (Edge)o;
            if (!other.getTo().equals(to)) {
                return false;
            }
            if (!other.getFrom().equals(from)) {
                return false;
            }
            if (!other.getLabel().equals(label)) {
                return false;
            }
            if (!other.getSequence().equals(sequence)) {
                return false;
            }
            if (!other.getCustomfieldconfig().equals(customfieldconfig)) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
    
    private Vertex from;
    private Vertex to;
    private String label;
    private Long sequence;

    /**
     * The sequence of an edge is not unique without the context of the from
     * vertext. This is that.
     */
    private Long customfieldconfig;
}

class Path {

    public Path() {
        this.edges = new ArrayList<Edge>();
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void addEdge(Edge e) {
        edges.add(e);
    }

    public void addEdges(Path path) {
        if (edges.isEmpty()) {
            edges = new ArrayList<Edge>(path.getEdges());
        } else {
            edges.addAll(path.getEdges());
        }
    }

    public List<String> getEdgeLabels() {
        List<String> result = new ArrayList<String>();
        for (Edge edge: edges) {
            result.add(edge.getLabel());
        }
        return result;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (Iterator it=edges.iterator(); it.hasNext();) {
            Edge edge = (Edge)it.next();
            sb.append(edge.getLabel());
            if (it.hasNext()) {
                sb.append("->");
            } else {
                //sb.append("-|");
            }
        }
        return sb.toString();
    }

    public boolean equals(Object o) {
        if (o instanceof Path) {
            Path other = (Path)o;
            if (other.getEdges().equals(edges)) {
                return true;
            }
        }
        return false;
    }
    
    private List<Edge> edges;
}

/**
 *  A comparator for keeping edges sorted by their sequence within a context.
 */
class EdgeComparator implements Comparator {

    public int compare(Object aObj, Object bObj) {
        // Object is the key of the TreeMap
        Edge a = (Edge) aObj;
        Edge b = (Edge) bObj;
        String aKey = a.getCustomfieldconfig() + "_" + a.getSequence();
        String bKey = b.getCustomfieldconfig() + "_" + b.getSequence();
        return aKey.compareTo(bKey);
    }
}
