package net.customware.jira.plugins.mdsf;

import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.comparator.LocaleSensitiveStringComparator;
import com.atlassian.jira.issue.customfields.MultipleSettableCustomFieldType;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.web.action.admin.customfields.AbstractEditConfigurationItemAction;
import org.ofbiz.core.entity.GenericValue;

import java.util.*;

/**
 * This class is the Action class that lets a user choose the Select-type
 * custom fields that are the dependent fields for an MDSF type custom field.
 */
public class EditMdsfAction extends AbstractEditConfigurationItemAction {

    private CustomFieldManager customFieldManager;
    private final FieldConfigSchemeManager schemeManager;
    private final OptionsManager optionsManager;
    private final ReindexMessageManager reindexMessageManager;
    private final IssueManager issueManager;

    private Map customFieldValuesHolder = new HashMap();
    private Options options;

    public EditMdsfAction(ManagedConfigurationItemService managedConfigurationItemService,
	                      CustomFieldManager customFieldManager,
                          FieldConfigSchemeManager schemeManager,
                          OptionsManager optionsManager,
                          IssueManager issueManager) {
        super(managedConfigurationItemService);
        this.customFieldManager = customFieldManager;
        this.schemeManager = schemeManager;
        this.optionsManager = optionsManager;
        this.issueManager = issueManager;
        this.reindexMessageManager = null; //TODO find a component-import for this?;
    }

    public String doDefault() throws Exception {
        log.debug("Entering doDefault");
        setReturnUrl(null);
        return super.doDefault();
    }

    protected void doValidation() {
        log.debug("Entering doValidation");
        String fieldId = getChangefield();
        CustomField cf = customFieldManager.getCustomFieldObject(fieldId);
        if (cf == null) {
            String msg = "Custom field " + fieldId + " does not exist";
            addErrorMessage(msg);
        }
    }

    protected String doExecute() throws Exception {
        log.debug("Entering doExecute");
        return INPUT;
    }

    /**
     * The set of dependent fields that are used by an MDSF field
     * are stored in the PropertySet table, not Options and the customfieldoption table.
     */
    public String doAddfield() {
        doValidation();
        if (invalidInput()) {
            return ERROR;
        }
        String fieldId = getChangefield();
        CustomField cf = customFieldManager.getCustomFieldObject(fieldId);
        CustomField mdsf_cf = getCustomField();
        Set currentFields = new HashSet(getCurrentFields());

        if (currentFields.contains(cf)) {
            log.debug("Field " + fieldId + " (" + cf.getName() + ") is already part of the MDSF custom field " + mdsf_cf.getName());
            return INPUT;
        }

        log.debug("Adding the field " + fieldId + " (" + cf.getName() + ") to the MDSF custom field " + mdsf_cf.getName());
        // This is the select custom field ids in order
        String currentFieldText = MdsfDAO.getCurrentFieldText(getFieldConfig());
        if (currentFieldText == null) {
            currentFieldText = fieldId;
        } else {
            currentFieldText = currentFieldText + MdsfDAO.FIELD_SEP + fieldId;
        }
        MdsfDAO.setCurrentFieldText(getFieldConfig(), currentFieldText);

        // TODO redirect or clear parameter values to avoid multiple adds
        return INPUT;
    }

    public String doChangefield() {
        doValidation();
        if (invalidInput()) {
            return ERROR;
        }
        String fieldId = getChangefield();
        CustomField cf = customFieldManager.getCustomFieldObject(fieldId);
        CustomField mdsf_cf = getCustomField();
        String op = getOp();

        log.debug("Changing the field " + fieldId + " (" + cf.getName() + ") for the custom field " + mdsf_cf.getName() + ", operation: " + op);
        String currentFieldText = MdsfDAO.getCurrentFieldText(getFieldConfig());
        String updatedFieldText = "";
        // remove or reorder
        String[] currentFieldIds = currentFieldText.split(MdsfDAO.FIELD_SEP);
        if (op.equals("remove")) {
            for (String fId: currentFieldIds) {
                if (!fId.equals(fieldId)) {
                    updatedFieldText = updatedFieldText + MdsfDAO.FIELD_SEP + fId;
                }
            }
            // TODO the removed field is not removed from the combos, just no
            // TODO this may be a problem with checking that an option string is valid, so may have to remove them
        } else if (op.equals("up")) {
            for (int i=0; i<currentFieldIds.length; i++) {
                log.debug("BEFORE " + i + ": " + currentFieldIds[i]);
            }
            for (int i=0; i<currentFieldIds.length; i++) {
                String fId = currentFieldIds[i];
                if (fId.equals(fieldId)) {
                    if (i > 0) {
                        String prev = currentFieldIds[i-1];
                        currentFieldIds[i-1] = fId;
                        currentFieldIds[i] = prev;
                        log.debug("Swapped " + fId + " and " + prev);
                    }
                }
            }
            for (int i=0; i<currentFieldIds.length; i++) {
                log.debug("AFTER " + i + ": " + currentFieldIds[i]);
            }
            for (String currentFieldId : currentFieldIds) {
                updatedFieldText = updatedFieldText + MdsfDAO.FIELD_SEP + currentFieldId;
            }
        } else if (op.equals("down")) {
            for (int i=0; i<currentFieldIds.length; i++) {
                String fId = currentFieldIds[i];
                if (fId.equals(fieldId)) {
                    if (i < currentFieldIds.length - 1) {
                        String next = currentFieldIds[i+1];
                        currentFieldIds[i+1] = fId;
                        currentFieldIds[i] = next;
                        i++;
                        log.debug("Swapped " + fId + " and " + next);
                    }
                }
            }
            for (String currentFieldId : currentFieldIds) {
                updatedFieldText = updatedFieldText + MdsfDAO.FIELD_SEP + currentFieldId;
            }
        } else {
            String msg = "Unknown MDSF field configuration operation " + op;
            log.warn(msg);
            addErrorMessage(msg);
            return ERROR;
        }
        log.debug("Updated field text is |" + updatedFieldText + "|");
        MdsfDAO.setCurrentFieldText(getFieldConfig(), updatedFieldText.trim());

        return INPUT;
    }

    /**
     * Add a combination made up of one option from the dependent fields.
     * Combinations are stored as options for the MDSF field.
     * None is stored as a value of -1.
     */
    public String doAddcombo() {
        // TODO check for admin permission
        log.debug("Adding a combination to the custom field " + getCustomField().getName());
        // Can't use simple accessor for these multiple values
        TreeMap<String, String> data = new TreeMap<String, String>();
        for (Enumeration e =  request.getParameterNames(); e.hasMoreElements() ;) {
            String name = (String)e.nextElement();
            if (!name.startsWith("customfield_")) {
                continue;
            }

            String[] values = request.getParameterValues(name);
            if (values == null || values.length == 0) {
                continue;
            } else if (values.length > 1) {
                log.warn("Multiple Select type fields are not supported for MDSF fields");
                continue;
            }

            log.debug("doAddcombo: found a field " + name + " in the passed parameters with value " + values[0]);
            data.put(name, values[0]);
        }

        boolean allNone = true;
        for (String s: data.values()) {
            if (!s.equals("-1")) {
                allNone = false;
            }
        }
        if (allNone) {
            // TODO i18n addErrorMessage(getText("mdsf.all.none.invalid"));
            addErrorMessage("The all 'None' combination is automatically added and cannot be manually added");
            return ERROR;
        }

        String value = MdsfDAO.convertMapToOptionString(data);
        Options options = getOptions();
        if (options.getOptionForValue(value, null) != null)
        {
            addErrorMessage(getText("admin.errors.customfields.value.already.exists"));
            return ERROR;
        }
        options.addOption(null, value);
        log.debug(getCustomField().getName() + " added a new option: " + value);
        return INPUT;
    }

    private Long comboid;

    public void setComboid(Long value)
    {
        this.comboid = value;
    }

    public Long getComboid()
    {
        return comboid;
    }

    /**
     * Remove a combination made up of one option from the dependent fields.
     * Combinations are stored as options for the MDSF field.
     */
    public String doRemovecombo() {
        // TODO check for admin permission
        Long id = getComboid();
        log.debug("Removing the combination " + id + " from the custom field " + getCustomField().getName());
        Options options = getOptions();
        Option option = options.getOptionById(id);

        if (option == null) {
            addErrorMessage(getText("admin.errors.customfields.invalid.select.list.value"));
            return ERROR;
        }

        boolean allNone = true;
        Map<String, String> data = MdsfDAO.convertOptionStringToMap(option.getValue());
        for (String s: data.values()) {
            if (!s.equals("-1")) {
                allNone = false;
            }
        }
        if (allNone) {
            // TODO i18n addErrorMessage(getText("mdsf.all.none.invalid"));
            addErrorMessage("Cannot remove the all 'None' combination");
            return ERROR;
        }

        // TODO remove from all issues only after warning
        removeValuesFromIssues(option);

        optionsManager.deleteOptionAndChildren(option);
        return INPUT;
    }

    /**
     * Add a reference to an existing MLCS custom field. This causes
     * all the MLCS options to be added as MDSF combos. Specifying
     * None does not remove previous combos; these have to be
     * explicitly removed.
     */
    public String doAddmlcsfield() {
        // TODO check for admin permission

        String value = getMlcsfield();
        if (value == null || value.equals("")) {
            // Issues will no longer look for a matching MLCS value
            // TODO could do this with a separate button too
            String msg = "Clearing the MLCS field value for field config " + getFieldConfig();
            log.debug(msg);
            MdsfDAO.clearMlcsField(getFieldConfig());
            return INPUT;
        }

        String prefix = "customfield_";
        if (!value.startsWith(prefix)) {
            String msg = "Expected MLCS field id format as customfield_DDDDD";
            log.warn(msg);
            addErrorMessage(msg);
            return INPUT;
        }
        value = value.substring(prefix.length());

        // MDSF 4.3.2 added support for contexts in MLCS fields
        String fieldid = value;
        String contextid = "all";
        // Changed from "fieldid" to "fieldid_contextid"
        int ctx_idx = value.indexOf("_");
        if (ctx_idx != -1) {
            fieldid = value.substring(0, ctx_idx);
            // Maybe id or "all"
            contextid = value.substring(ctx_idx+1);
        }

        try {
            Long check = Long.parseLong(fieldid);
        } catch (NumberFormatException nfe) {
            String msg = "MLCS custom field id does not end with an integer: " + fieldid;
            log.error(msg);
            addErrorMessage(msg);
            return INPUT;
        }

        try {
            if (!contextid.equals("all")) {
                Long check = Long.parseLong(contextid);
            }
        } catch (NumberFormatException nfe) {
            String msg = "MLCS custom field context id does not end with an integer: " + contextid;
            log.error(msg);
            addErrorMessage(msg);
            return INPUT;
        }
        // Store it in the database
        MdsfDAO.setMlcsField(getFieldConfig(), value);

        String storedValue = MdsfDAO.getMlcsField(getFieldConfig());
        if (storedValue == null) {
            return INPUT;
        }

        // TODO add a checkbox to change truncate?
        addAllMlcsOptions(fieldid, contextid, getTruncate());
        if (hasAnyErrors()) {
            String msg = "Unable to import values from MLCS custom field and context: " + value;
            log.error(msg);
            MdsfDAO.clearMlcsField(getFieldConfig());
            return INPUT;
        }

        return INPUT;
    }

    /**
     *
     * @return return the INPUT page
     */
    public String doPopulateToDependentField(){
        String populateToDependentField = request.getParameter("PopulateToDependentField");
        if(null == populateToDependentField){
            MdsfDAO.setPopulateToDependentField(getFieldConfig(), false);
            return INPUT;
        }
        if(populateToDependentField.equals("on")){
            MdsfDAO.setPopulateToDependentField(getFieldConfig(), true);
        }
        return INPUT;
    }

    public boolean isPopulateToDependentField(){
        return MdsfDAO.getPopulateToDependentField(getFieldConfig());
    }

    /**
     * Extract the MLCS options from the chosen custom field and create MDSF
     * options for each one.
     */
    private void addAllMlcsOptions(String customFieldId, String customFieldContextId, boolean truncate) {
        List<List<String>> mlcsOptionValues = getAllMlcsOptionValues(customFieldId, customFieldContextId, truncate);
        if (hasAnyErrors()) {
            // Most likely error is a too-long MLCS option
            // Don't add any options until the errors are resolved
            return;
        }

        for (List<String> mlcsValue: mlcsOptionValues) {
            // The options in the dependent fields have to exist before we
            // can construct the MDSF option string
            addDependentOptions(mlcsValue);
            // Refresh each pass
            Options options = getOptions();
            String value = MdsfDAO.convertMlcsValueToOptionString(mlcsValue, getFieldConfig());
            if (value == null) {
                log.warn("Unable to convert " + mlcsValue + " to MDSF option string");
            }
            // This is a case-insensitive search
            if (options.getOptionForValue(value, null) != null) {
                log.debug("MDSF option " + value + " already exists");
                continue;
            }
            options.addOption(null, value);

            log.info(getCustomField().getName() + " added a new option from MLCS: " + value + " = " + mlcsValue);
        }
    }

    /**
     * Ensure that each value in mlcsValue exists in the appropriate
     * dependent field.
     * @return a Map of custom field ids to option values, as used
     * by MdsfDAO.convertMapToOptionString()
     */
    private void addDependentOptions(List<String> mlcsValue) {
        List<CustomField> currentFields = getCurrentFields();
        for (int i=0; i<mlcsValue.size(); i++) {
            CustomField cf = (CustomField)currentFields.get(i);
            String mlcsSubValue = (String)mlcsValue.get(i);

            Options options = MdsfDAO.getFieldOptions(cf.getId());
            Option option = options.getOptionForValue(mlcsSubValue, null);
            // Add a new option if it doesn't exist. None values are never added to dependent select fields.
            if (!mlcsSubValue.equals(MdsfDAO.NONE_LABEL) && option == null) {
                option = options.addOption(null, mlcsSubValue);
            }
        }
    }

    /**
     * Get the string values of all MLCS options for the given field.
     * The values are not removed after import, unlike MdsfCFType
     * getComboFromMlcs().  Set an error if the MLCS value has more
     * fields that the MDSF field. Pad shorter values out with None.
     *
     * Note that MLCS fields support subset of their options, so
     * if X,X3,X333 is valid, then X,X3 is valid as well.
     *
     * @return an Array of MDSF option strings arrays,
     * e.g. customfield_NNNNN_1041 or customfield_NNNNN_-1
     * from the customfieldoption table.
     */
    private List<List<String>> getAllMlcsOptionValues(String customFieldId,
                                                      String customFieldContextId,
                                                      boolean truncate) {
        List<List<String>> result = new ArrayList<List<String>>();

        DAG dag = new DAG();
        List mlcsValues = MdsfDAO.getAllMlcsOptions(new Long(customFieldId), customFieldContextId);
        for (Iterator it = mlcsValues.iterator(); it.hasNext(); ) {
            GenericValue gv = (GenericValue)it.next();
            Long id = gv.getLong("id");
            Long parent = gv.getLong("parentoptionid");
            // Use the field name as defined in entitymodel.xml not the DB column name
            String value = gv.getString("value");
            Long sequence = gv.getLong("sequence");
            if (parent == null) {
                parent = new Long(-1);
            }
            // For debugging
            Long customfield = gv.getLong("customfield");
            Long customfieldconfig = gv.getLong("customfieldconfig");

            dag.addEdge(parent.toString(), id.toString(), value, sequence, customfield, customfieldconfig);
        }

        // Create Strings with keys of the current fields and the
        // desired value for each edge.
        List<Path> paths = dag.getPaths();
        if (!paths.isEmpty()) {
            log.debug("Importing the following MLCS options:");
            StringBuffer sb = new StringBuffer();
            for (Path path: paths) {
                sb.append("  ");
                sb.append(path.getEdgeLabels());
                sb.append("\n");
            }
            log.debug("End of list of imported MLCS options");
            log.debug(sb.toString());
        }

        for (Path path: paths) {
            List<String> rawMlcsValue = path.getEdgeLabels();
            log.debug("Path: " + rawMlcsValue);

            List<String> mlcsValue = MdsfDAO.adjustMlcsValue(rawMlcsValue, getFieldConfig(), truncate);
            if (mlcsValue == null) {
                String msg = "Unable to adjust MLCS value: " + rawMlcsValue;
                if (!getTruncate()) {
                    msg = "More levels in the MLCS value than dependent MDSF fields currently configured: " + rawMlcsValue;
                }
                log.warn(msg);
                addErrorMessage(msg);
                continue;
            }
            result.add(mlcsValue);
        }
        return result;
    }

    // From EditCustomFieldOptions
    private void removeValuesFromIssues(Option option)
    {
        Collection issues = getAffectedIssues(option);
        for (Object issue1 : issues) {
            Issue issue = (Issue) issue1;
            MultipleSettableCustomFieldType customFieldType = (MultipleSettableCustomFieldType) getCustomField().getCustomFieldType();
            customFieldType.removeValue(getCustomField(), issue, option);
        }
        if (reindexMessageManager != null) {
            reindexMessageManager.pushMessage(getRemoteUser(), "admin.notifications.task.custom.fields");
        }
    }

    // From EditCustomFieldOptions
    public Collection getAffectedIssues(Option option)
    {
        final MultipleSettableCustomFieldType customFieldType = (MultipleSettableCustomFieldType) getCustomField().getCustomFieldType();
        Collection ids = customFieldType.getIssueIdsWithValue(getCustomField(), option);
        Collection issues = new ArrayList(ids.size());

        for (Object id1 : ids) {
            Long id = (Long) id1;
            final Issue issue = IssueImpl.getIssueObject(issueManager.getIssue(id));
            final FieldConfig relevantConfigFromGv = getCustomField().getRelevantConfig(issue);
            if (getFieldConfig().equals(relevantConfigFromGv)) {
                issues.add(issue);
            }
        }
        return issues;
    }

    private String changeFieldValue;
    public void setChangefield(String value) {
        this.changeFieldValue = value;
    }
    public String getChangefield() {
        return this.changeFieldValue;
    }

    private String op;
    public void setOp(String value) {
        this.op = value;
    }
    public String getOp() {
        return this.op;
    }

    private String mlcsField;

    public void setMlcsfield(String value) {
        // Persisted in doAddmlcsfield
        log.debug("Setting MLCS field from raw value: " + value);
        mlcsField = value;
    }

    public String getMlcsfield() {
        // Not the persisted value
        return mlcsField;
    }

    public boolean currentMlcsfield(String value) {
        if (mlcsField == null || mlcsField.equals("")) {
            return false;
        }
        return mlcsField.equals(value);
    }

    /**
     * Provide a map of field id to field name for custom fields with the
     * MLCS key. These custom fields are not currently valid fields in JIRA.
     */
    public Map<String, String> getMlcsfields() {
        Map<String, String> result = new TreeMap<String, String>(new LocaleSensitiveStringComparator(getLocale()));
        List<GenericValue> mlcsFields = MdsfDAO.getMlcsFields();
        for (GenericValue mlcsField: mlcsFields) {
            List<GenericValue> contexts = MdsfDAO.getMlcsFieldContexts(mlcsField);
            // Add the case for all options from all contexts
            String key = "customfield_" + mlcsField.getString("id").toString() + "_all";
            // TODO i18n
            String value = mlcsField.getString("name") + " - All";
            result.put(key, value);

            for (GenericValue context: contexts) {
                // TODO toString not needed? and elsewhere
                key = "customfield_" + mlcsField.getString("id").toString() + "_" + context.getString("id");
                value = mlcsField.getString("name") + " (" + context.getString("name") + ")";
                result.put(key, value);
            }
        }
        return result;
    }

    public Collection<Combo> getCurrentCombos()
    {
        return MdsfDAO.getCurrentCombos(getFieldConfig());
    }

    public List<CustomField> getCurrentFields()
    {
        return MdsfDAO.getCurrentFields(getFieldConfig());
    }

    public Options getFieldOptions(String fieldId)
    {
        return MdsfDAO.getFieldOptions(fieldId);
    }


    /**
     * Return the fields that can be used as dependent fields for a MDSF field.
     * Fields that have already been used can be used again.
     *
     * Adding a field twice to a single MDSF field should work but is
     * hard to remove (all such field instances are removed but then
     * reappear). For now, duplicate fields are prevented.
     *
     * @return a set of custom fields, sorted by custom field name
     */
    public Set<CustomField> getPossibleFields() {
        log.debug("Entering getPossibleFields");
        Set<CustomField> result = new TreeSet<CustomField>(new CustomFieldNameComparator());
        List<CustomField> customFields = customFieldManager.getCustomFieldObjects();
        Set currentFields = new HashSet(getCurrentFields());

        for (CustomField cf: customFields) {
            // Checking for MultipleSettableCustomFieldType passes
            // Multiselect and Cascading Select as well
            boolean isSelect = cf.getCustomFieldType().getKey().endsWith(":select");
            boolean isMdsf = cf.getCustomFieldType() instanceof MdsfCFType;
            boolean isAlreadyPresent = currentFields.contains(cf);

            // Dependent fields have to have one global context
            boolean singleContext = false;
            List schemes = schemeManager.getConfigSchemesForField(cf);
            // If a field has no context, it never appears
            if (schemes.size() == 1) {
                singleContext = true;
            }

            if (isSelect && !isMdsf && singleContext && !isAlreadyPresent) {
                result.add(cf);
            }
        }
        return result;
    }

    /**
     * Keep a cached instance of the options for this field
     * TODO what about for different field configs?
     * TODO caching not working?
     */
    protected Options getOptions() {
        options = getCustomField().getOptions(null, getFieldConfig(), null);
        return options;
    }

    /**
     * If true, then imported MLCS values are truncated to fit the
     * current size of the MDSF field. If false, then importing from
     * an MLCS field will not proceed.
     */
    private boolean truncate = true;

    public void setTruncate(boolean value) {
        truncate = value;
    }

    public boolean getTruncate() {
        return truncate;
    }

}

/**
 * A comparator for keeping entries in a map sorted
 * OLD : by their
 * String value.
 * @see <a href="http://www.coderanch.com/t/370496/java/java/Sorting-Hashtable-values-retriving-kay">sorting-hashtable</a>
 */
class CustomFieldNameComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        CustomField e1 = (CustomField)o1 ;
        CustomField e2 = (CustomField)o2 ;
        String first = e1.getName();
        String second = e2.getName();
        return first.compareTo(second);
    }
}
