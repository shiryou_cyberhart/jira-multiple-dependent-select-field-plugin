package net.customware.jira.plugins.mdsf;

import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValue;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter.MappedCustomFieldValue;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;

/**
 * Implements ProjectCustomFieldImporter for the MDSF custom field type.
 *
 * @since v3.13
 */
public class MdsfCustomFieldImporter implements ProjectCustomFieldImporter
{
    private final OptionsManager optionsManager;

    public MdsfCustomFieldImporter(final OptionsManager optionsManager)
    {
        this.optionsManager = optionsManager;
    }

    public MessageSet canMapImportValue(final ProjectImportMapper projectImportMapper, final ExternalCustomFieldValue customFieldValue, final FieldConfig fieldConfig, final I18nHelper i18n)
    {
        final String value = customFieldValue.getValue();
        // Get this custom field's "valid" options and see if the value is one of them
        final Options options = optionsManager.getOptions(fieldConfig);
        if (options.getOptionForValue(value, null) == null)
        {
            // If an option does not exist for the value we are looking at then log an error and stop the import
            final MessageSet messageSet = new MessageSetImpl();
            final String customFieldName = fieldConfig.getCustomField().getName();
            messageSet.addErrorMessage(i18n.getText("admin.errors.project.import.custom.field.option.does.not.exist", customFieldName, value));
            messageSet.addErrorMessageInEnglish("The custom field '" + customFieldName + "' requires option '" + value + "' for the import but it does not exist in the current JIRA instance.");
            return messageSet;
        }
        return null;
    }

    public MappedCustomFieldValue getMappedImportValue(final ProjectImportMapper projectImportMapper, final ExternalCustomFieldValue customFieldValue, final FieldConfig fieldConfig)
    {
        // Since this method will never be called without a successful call to canMap we can safely just pass the
        // value back out.
        return new MappedCustomFieldValue(customFieldValue.getValue());
    }
}
