package net.customware.jira.plugins.mdsf;

import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.velocity.htmlsafe.HtmlSafe;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The main transport class that represents an MDSF combination.
 */
public class Combo implements Comparable {

    String baseurl;

    public Combo(Map map) {
        this.valuesMap = new TreeMap(map);
        this.optionsManager = ManagerFactory.getOptionsManager();
        this.currentFields = null;
        this.optionId = null;

        baseurl = MdsfDAO.getBaseUrl();
    }

    /**
     * Each dependent custom field id and its option id, sorted by
     * custom field id. An option id may be -1 for None.
     */
    private Map<String, String> valuesMap;

    public Map<String, String> getMap() {
        return valuesMap;
    }

    private OptionsManager optionsManager;

    /**
     * The MDSF option id for this combo. Optional.
     */
    private Long optionId;

    public void setOptionid(Long value) {
        this.optionId = value;
    }

    public Long getOptionid() {
        return optionId;
    }

    /**
     * The current fields in order for this combo. Optional.
     */
    private List<CustomField> currentFields;

    public void setCurrentfields(List<CustomField> value) {
        this.currentFields = value;
    }

    public List<CustomField> getCurrentfields() {
        return currentFields;
    }

    /**
     * @return the option id in this combo for the given field
     * id. This may be null if field is not present in the combo or is
     * stored as null.
     */
    public String getRawOptionId(String fieldId) {
        return valuesMap.get(fieldId);
    }

    /**
     * The same as getRawOptionId but returns string errors instead of null
     * for use in edit.vm
     *
     * @return the option id of the currently chosen value for the
     * given custom field in this Combo
     */
    public String getOptionId(String fieldId) {
        if (!valuesMap.containsKey(fieldId)) {
            return "Dependent field " + fieldId + " is not part of this MDSF custom field";
        }
        String value = getRawOptionId(fieldId);
        if (value == null) {
            // nulls are not supposed to be stored as an option
            return "Null value for " + fieldId;
        }
        return value;
    }

    /**
     * Return an array of the String values in this combination in the
     * same order as the current fields.  This assumes that the
     * currentFields have been set.
     */
    public String[] getRowId() {
        if (currentFields == null) {
            // TODO Display the fields in the default order of the TreeMap
            // but this will look odd?
            String msg = "currentFields was not set, displaying fields in default order";
            // log.error(msg);
            String[] row = new String[valuesMap.size()];
            for (int i=0; i< row.length; i++) {
                row[i] = "unknown";
            }
            return row;
        }

        String[] row = new String[currentFields.size()];
        for (int i=0; i< row.length; i++) {
            CustomField cf = currentFields.get(i);
            row[i] = valuesMap.get(cf.getId());
         }
         return row;
    }

    /**
     * Return an array of the String values in this combination in the
     * same order as the current fields.  This assumes that the
     * currentFields have been set.
     */
    public String[] getRow() {
        if (currentFields == null) {
            // TODO Display the fields in the default order of the TreeMap
            String msg = "CurrentFields was not set, displaying fields in default order";
            String[] row = new String[valuesMap.size()];
            for (int i=0; i< row.length; i++) {
                row[i] = "unknown";
            }
            return row;
        }

        String[] row = new String[currentFields.size()];
        for (int i=0; i< row.length; i++) {
            CustomField cf = currentFields.get(i);
            String optStr = valuesMap.get(cf.getId());
            if (optStr == null) {
                // There is no entry recorded for the current field in the combo data
                row[i] = " ";
            } else if (optStr.equals("-1")) {
                row[i] = "None"; // TODO i18n
            } else {
                Long optId = new Long(optStr);
                Option opt = optionsManager.findByOptionId(optId);
                if (opt == null) {
                    // TODO remove?
                    row[i] = " ";
                } else {
                    row[i] = opt.getValue();
                }
            }
         }
         return row;
    }

    /**
     * Return an representation that can be used directly in javascript.
     * [["value1", "text1"],
     *   ["value2", "text2"]]
     */
    @HtmlSafe
    public String getJSRow() {
        if (currentFields == null) {
            return "[]";
        }

        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i=0; i<currentFields.size(); i++) {
            CustomField cf = currentFields.get(i);
            String optStr = valuesMap.get(cf.getId());
            if (optStr == null) {
                // There is no entry recorded for the current field in the combo data
                sb.append("\n [\"\", \"\"]");
            } else if (optStr.equals("-1")) {
                String otherid = cf.getId() + "_" + "-1";
                String text = "None"; // TODO i18n
                sb.append("\n [\"" + otherid + "\", \"" + text + "\"]");
            } else {
                Long optId = new Long(optStr);
                Option opt = optionsManager.findByOptionId(optId);
                if (opt == null) {
                    sb.append("\n [\"\", \"\"]");
                } else {
                    // This format is the same as otherid in edit.vm
                    String otherid = cf.getId() + "_" + opt.getOptionId();
                    String text = opt.getValue();
                    // TODO htmlEncode?
                    sb.append("\n [\"" + otherid + "\", \"" + text + "\"]");
                }
            }
            if (i < currentFields.size() - 1) {
                sb.append(",");
            }
         }
        sb.append("\n]");
        return sb.toString();
    }

    public boolean isAllNone() {
        if (currentFields == null) {
            return true;
        }
        for (int i=0; i< currentFields.size(); i++) {
            CustomField cf = currentFields.get(i);
            String optStr = valuesMap.get(cf.getId());
            if (optStr != null && !optStr.equals("-1")) {
                return false;
            }
        }
        return true;
    }

    /**
     * Used to convert a Combo object to the contents of the field as
     * seen by the user in JIRA, i.e. value in the view.vm template.
     */
    public String toString() {
        String[] row = getRow();
        String valueSep = ", ";
        StringBuffer sb = new StringBuffer();

        // A, B is displayed as A, B
        // A, None is displayed as A
        // A, None, B is displayed as A, None, B
        // A, None, None is displayed as A
        int lastNone = row.length;
        for (int i=row.length-1; i>=0; i--) {
            // TODO i18n
            if (row[i].equals("None"))  {
                lastNone = i;
            } else {
                break;
            }
        }
        for (int i=0; i<row.length; i++) {
            if (i == lastNone) {
                break;
            }
            sb.append(row[i]);
            if (i < lastNone-1) {
                sb.append(valueSep);
            }
        }
        return sb.toString().trim();
    }

    public int compareTo(Object o) {
        String s = toString();
        return s.compareTo(o.toString());
    }

    /**
     * For the xml.vm template.
     * TODO not sure what renderedValue is or should be in this case
     */
    public String toXML() {
        return MdsfDAO.convertMapToOptionString(valuesMap);
    }

    public String dump() {
        StringBuffer sb = new StringBuffer();
        sb.append("raw data: ");
        sb.append(valuesMap);
        if (optionId != null) {
            sb.append(". optionId=");
            sb.append(optionId);
        }
        return sb.toString().trim();
    }
}