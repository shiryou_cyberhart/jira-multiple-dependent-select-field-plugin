package it.net.customware.jira.plugins.mdsf.pageobject;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * @author ianchiu
 * @since 20140206
 */
public class AssociateCustomFieldToScreenPage extends com.atlassian.jira.pageobjects.pages.admin.customfields.AssociateCustomFieldToScreenPage {

    @ElementBy (cssSelector = "input[value='1'][name='associatedScreens']")
    private CheckboxElement defaultScreenCheckbox;

    @ElementBy (id = "update_submit")
    private PageElement update;


    public void checkDefaultScreen() {
        defaultScreenCheckbox.check();
    }

    public void save() {
        update.click();
    }
}
