package it.net.customware.jira.plugins.mdsf.pageobject;

import com.atlassian.jira.pageobjects.pages.admin.customfields.TypeSelectionCustomFieldDialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * @author ianchiu
 * @since 20140207
 */
public class AddCustomFieldDialog extends TypeSelectionCustomFieldDialog {

    @ElementBy (className = "item-button:first-child")
    private PageElement all;

    public AddCustomFieldDialog clickAll() {
        all.click();
        return this;
    }
}
