package it.net.customware.jira.plugins.mdsf;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ViewCustomFields;
import com.atlassian.jira.testkit.beans.CustomFieldResponse;
import com.atlassian.pageobjects.TestedProductFactory;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import it.net.customware.jira.plugins.mdsf.pageobject.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;


public class MdsfTest {

    class CustomField {
        private String id;
        private String name;
        private Map<Integer,String> customfieldOptions = new HashMap<Integer,String>();

        public CustomField(String name){
            this.name = name;
            customfieldOptions.put(0,randomAlphabetic(8));
            customfieldOptions.put(1,randomAlphabetic(8));
            customfieldOptions.put(2,randomAlphabetic(8));
        }

        public String getCustomFieldOption(Integer key){
            return customfieldOptions.get(key);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }
    }

    static final JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class);

    String projectName = randomAlphabetic(8);
    String projectKey = randomAlphabetic(3).toUpperCase();

    CustomField firstSelectList = new CustomField(randomAlphabetic(8));
    CustomField secondSelectList = new CustomField(randomAlphabetic(8));
    CustomField thirdSelectList = new CustomField(randomAlphabetic(8));
    CustomField mdsfCustomField = new CustomField(randomAlphabetic(8));

    @Before public void setUp() {

        jira.backdoor().restoreBlankInstance();
        jira.quickLoginAsSysadmin();

        jira.backdoor().getTestkit().project().addProject(projectName, projectKey,"admin");
        setUpCustomField();
    }

    @Test public void userShouldBeAbleToSelectMdsfFieldCombination() {

        String issueKey = jira.backdoor().getTestkit().issues().createIssue(projectKey,"test").key;
        jira.goToViewIssue(issueKey).editIssue();

        EditIssueDialog editIssueDialog = jira.getPageBinder().bind(EditIssueDialog.class);
        editIssueDialog.setMdsfField(mdsfCustomField.getId(),firstSelectList.getId(),firstSelectList.getCustomFieldOption(0));

        String secondSelectFieldValueInMdsfCombination = secondSelectList.getCustomFieldOption(0);
        String secondSelectedValueText = editIssueDialog.findMdsfField(mdsfCustomField.getId(), secondSelectList.getId()).getText();

        assertThat(secondSelectFieldValueInMdsfCombination, equalTo(secondSelectedValueText));
    }

    @Test public void userShouldBeAbleToViewSelectedMdsfFieldCombination() {

        String issueKey = jira.backdoor().getTestkit().issues().createIssue(projectKey,"test").key;
        jira.goToViewIssue(issueKey).editIssue();

        EditIssueDialog editIssueDialog = jira.getPageBinder().bind(EditIssueDialog.class);
        editIssueDialog.setMdsfField(mdsfCustomField.getId(),firstSelectList.getId(),firstSelectList.getCustomFieldOption(0));
        editIssueDialog.submit();

        jira.goToViewIssue(issueKey);

        String firstMdsfCombinationOption = firstSelectList.getCustomFieldOption(0)+", "+secondSelectList.getCustomFieldOption(0)+", "+thirdSelectList.getCustomFieldOption(0);
        String MdsfFieldText = jira.getTester().getDriver().findElement(By.id("customfield_10000-val")).getText();

        assertThat(MdsfFieldText, equalTo(firstMdsfCombinationOption));
    }

    private void setUpCustomField() {

        createMdsfCustomField(mdsfCustomField);
        createCustomFieldSelectList(firstSelectList);
        createCustomFieldSelectList(secondSelectList);
        createCustomFieldSelectList(thirdSelectList);

        mdsfCustomField.setId(getCustomFieldId(mdsfCustomField.getName()));
        firstSelectList.setId(getCustomFieldId(firstSelectList.getName()));
        secondSelectList.setId(getCustomFieldId(secondSelectList.getName()));
        thirdSelectList.setId(getCustomFieldId(thirdSelectList.getName()));

        EditMdsfCustomFieldPage editMdsfCustomFieldPage = jira.visit(EditCustomFieldPage.class,mdsfCustomField.getId()).editCombination();

        // Selecting the three 'Select List' customfields and adding Combination
        editMdsfCustomFieldPage.addField(firstSelectList.getName());
        editMdsfCustomFieldPage.addField(secondSelectList.getName());
        editMdsfCustomFieldPage.addField(thirdSelectList.getName());

        selectMdsfOption(firstSelectList.getId(), firstSelectList.getCustomFieldOption(0));
        selectMdsfOption(secondSelectList.getId(), secondSelectList.getCustomFieldOption(0));
        selectMdsfOption(thirdSelectList.getId(), thirdSelectList.getCustomFieldOption(0));
        editMdsfCustomFieldPage.addCombination();

        selectMdsfOption(firstSelectList.getId(), firstSelectList.getCustomFieldOption(1));
        selectMdsfOption(secondSelectList.getId(), secondSelectList.getCustomFieldOption(1));
        selectMdsfOption(thirdSelectList.getId(),thirdSelectList.getCustomFieldOption(1));
        editMdsfCustomFieldPage.addCombination();

        selectMdsfOption(firstSelectList.getId(),firstSelectList.getCustomFieldOption(2));
        selectMdsfOption(secondSelectList.getId(),secondSelectList.getCustomFieldOption(2));
        selectMdsfOption(thirdSelectList.getId(),thirdSelectList.getCustomFieldOption(2));
        editMdsfCustomFieldPage.addCombination();

        editMdsfCustomFieldPage.save();
    }

    private void createMdsfCustomField(CustomField mdsfCustomField) {

        jira.goTo(ViewCustomFields.class)
                .addCustomField();

        jira.getPageBinder().bind(AddCustomFieldDialog.class)
                .clickAll()
                .select("MDSF - Multiple Dependent Select Field")
                .next()
                .name(mdsfCustomField.getName())
                .nextAndThenAssociate();

        AssociateCustomFieldToScreenPage associationPage = jira.getPageBinder().bind(AssociateCustomFieldToScreenPage.class);

        associationPage.checkDefaultScreen();
        associationPage.save();
    }

    private void createCustomFieldSelectList(CustomField customField) {

        jira.goTo(ViewCustomFields.class)
                .addCustomField()
                .select("Select List (single choice)")
                .next()
                .name(customField.getName())
                .addOption(customField.getCustomFieldOption(0))
                .addOption(customField.getCustomFieldOption(1))
                .addOption(customField.getCustomFieldOption(2))
                .nextAndThenAssociate();

        jira.getPageBinder().bind(AssociateCustomFieldToScreenPage.class).save();
    }

    private String getCustomFieldId(final String customFieldName) {

        List<CustomFieldResponse> customFields = jira.backdoor().customFields().getCustomFields();
        CustomFieldResponse response = Iterables.find(customFields, new Predicate<CustomFieldResponse>() {
            @Override
            public boolean apply(final CustomFieldResponse input) {
                return customFieldName.equals(input.name);
            }
        });

        return response.id.substring(12);
    }

    private void selectMdsfOption(String customfieldId, String option) {

        WebElement selectList = jira.getTester().getDriver().findElement(By.id("customfield_" + customfieldId));

        for(WebElement selection:selectList.findElements(By.tagName("option"))){
            if(selection.getText().equals(option)){
                selection.click();
                break;
            }
        }
    }
}