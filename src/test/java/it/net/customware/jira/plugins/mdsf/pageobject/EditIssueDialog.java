package it.net.customware.jira.plugins.mdsf.pageobject;

import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

/**
 * @author ianchiu
 * @since 20140211
 */
public class EditIssueDialog extends com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog {

    public EditIssueDialog setMdsfField(String mdsfFieldId, String selectFieldId, String value) {
        findOption(findMdsfField(mdsfFieldId, selectFieldId), value).click();
        return this;
    }

    public PageElement findMdsfField(String mdsfFieldId, String selectFieldId){
        return locator.find(By.id("customfield_"+mdsfFieldId+"_customfield_"+selectFieldId));
    }

    private PageElement findOption(PageElement mdsfField, String value) {
        for(PageElement option:mdsfField.findAll(By.tagName("option"))) {
            if(option.getText().equals(value)){
                return option;
            }
        }
        return mdsfField;
    }
}
