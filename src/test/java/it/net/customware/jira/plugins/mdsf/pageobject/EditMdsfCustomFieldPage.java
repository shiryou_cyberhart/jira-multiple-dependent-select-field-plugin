package it.net.customware.jira.plugins.mdsf.pageobject;

import com.atlassian.pageobjects.elements.*;

public class EditMdsfCustomFieldPage {


    @ElementBy(name = "Cancel")
    private PageElement submit;

    @ElementBy(cssSelector = "*[value=\"Add Field\"]")
    private PageElement addField;

    @ElementBy(id = "changefield")
    private SelectElement selectField;

    @ElementBy(cssSelector = "*[value=\"Add Combination\"]")
    private PageElement addCombination;

    public EditMdsfCustomFieldPage addField(String option) {
        selectField.select(Options.text(option));
        addField.click();
        return this;
    }

    public EditMdsfCustomFieldPage addCombination() {
        addCombination.click();
        return this;
    }

    public EditMdsfCustomFieldPage save() {
        submit.click();
        return this;
    }

}