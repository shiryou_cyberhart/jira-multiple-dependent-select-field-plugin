package it.net.customware.jira.plugins.mdsf.pageobject;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * @author ianchiu
 * @since 20140207
 */
public class EditCustomFieldPage extends AbstractJiraPage {

    @ElementBy (linkText = "Edit MDSF Fields and Combinations")
    private PageElement editCombination;

    private String customFieldId;

    public EditCustomFieldPage(String customFieldId) {
        this.customFieldId = customFieldId;
    }

    public EditMdsfCustomFieldPage editCombination() {
        editCombination.click();
        return pageBinder.bind(EditMdsfCustomFieldPage.class);
    }

    @Override
    public TimedCondition isAt() {
        return editCombination.timed().isVisible();
    }

    @Override
    public String getUrl() {
        return "/secure/admin/ConfigureCustomField!default.jspa?customFieldId="+customFieldId;
    }
}
