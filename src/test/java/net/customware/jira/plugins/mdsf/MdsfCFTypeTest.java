package net.customware.jira.plugins.mdsf;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.Project;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MdsfCFTypeTest {

    /**
     * Extending the existing MockIssue class can make the test closer to the real implementation
     * The annotation Mock will lost other function which is already defined in MockIssue, which is been used in this test.
     */
    class MockIssue extends com.atlassian.jira.mock.issue.MockIssue {

        Map<CustomField, Object> customFieldValues;

        public MockIssue() {
            super();
            this.customFieldValues = new HashMap<CustomField, Object>();
        }

        @Override
        public void setCustomFieldValue(CustomField customField, Object value) {
            ModifiedValue modifiedValue = new ModifiedValue(getCustomFieldValue(customField), value);
            customFieldValues.put(customField, value);
            getModifiedFields().put(customField.getId(), modifiedValue);
        }

        @Override
        public Object getCustomFieldValue(CustomField customField) {
            if (!customFieldValues.containsKey(customField)) {
                if (getGenericValue() != null) {
                    customFieldValues.put(customField, customField.getValue(this));
                }
                else {
                    customFieldValues.put(customField, customField.getDefaultValue(this));
                }
            }
            return customFieldValues.get(customField);
        }
    }

    MdsfCFType $;

    @Mock OptionsManager optionsManager;

    @Mock FieldConfig fieldConfig;
    @Mock CustomField customField;
    @Mock Options options;
    MockOption option;

    MockIssue issue = new MockIssue();

    @Before public void setUpIssue() {

        issue = spy(new MockIssue());

        // Issue type mocking is needed to not break when validation on the screen happens.
        IssueType issueType = mock(IssueType.class);
        doReturn(issueType).when(issue).getIssueTypeObject();
        doReturn("").when(issueType).getId();
    }

    @Before public void setupCustomFieldAndOptions() {

        $ = new MdsfCFType(null, null, optionsManager, null, null);

        Map<String, Object> fields = new HashMap<String, Object> () {{
            put("id", new Long(randomNumeric(2)));
            put("sequence", new Long(randomNumeric(2)));
            put("value", randomAlphabetic(4));
            put("customfieldconfig", fieldConfig.getId());
            put("parentoptionid", null);
            put("type", null);
            put("disabled", false);
            put("customfield", customField.getId());
        }};
        option = new MockOption(new MockGenericValue("CustomFieldOption", fields));
    }

    /**
     * @see net.customware.jira.plugins.mdsf.MdsfCFType#updateSelectField(com.atlassian.jira.issue.Issue, com.atlassian.jira.issue.fields.CustomField, String)
     */
    @Test public void updateMdsfShouldCloneTheValueToRelatedSelectField() {

        String optionValue = (String) option.getGenericValue().get("value");

        when(customField.isInScope((Project) anyObject(), anyListOf(String.class))).thenReturn(true);
        when(customField.getRelevantConfig((IssueContext) anyObject())).thenReturn(fieldConfig);
        when(optionsManager.getOptions(fieldConfig)).thenReturn(options);
        when(options.getOptionForValue(optionValue, null)).thenReturn(option);

        $.updateSelectField(issue, customField, optionValue);
        String actualResult = issue.getCustomFieldValue(customField).toString();

        assertThat(optionValue, is(actualResult));
    }
}
