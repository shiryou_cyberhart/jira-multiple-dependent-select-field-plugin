
                   Multiple Dependent Select Field (MDSF)


Matt Doar
CustomWare

Build
-----
You need Apache Maven to build this project. Two of dependencies, namely `jndi` and `jta` needs to be installed manually on your
local Maven repository prior to building the project. You can download the `jndi` version `1.2.1` from 
[Java Platform Technology Downloads Page](http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html)
and then install it using the following command:

    mvn install:install-file -DgroupId=jndi -DartifactId=jndi -Dversion=1.2.1 -Dpackaging=zip -Dfile=<path-to-file>
    
You can download the [jta](http://www.java2s.com/Code/Jar/j/Downloadjta101jar.htm) version `1.0.1`
and then install it using the following command:

    mvn install:install-file -DgroupId=jta -DartifactId=jta -Dversion=1.0.1 -Dpackaging=jar -Dfile=<path-to-file>

For more information please refer to [this guide](https://maven.apache.org/guides/mini/guide-coping-with-sun-jars.html).

    
Overview
--------

JIRA provides a drop-down select list field type, where a user can
select from a range of pre-determined values. JIRA also provides a
field type "Cascading Select" which appears to the user as two select
fields and the values available in the field on the right depend on
the value that was chosen in the field on the left.

The MDSF plugin allows JIRA admins to create fields that offer more
than two levels of selection. The values offered for each level depend
upon the values chosen for fields to the left of the current field.

The MDSF plugin also supports using options that were already
configured for MLCS (Multi Level Cascade Select) custom fields

System Requirements
-------------------

This plugin has been tested with Chrome 5.0, 6.0 and Firefox 3.5+.

There is a known problem with Firefox 3.0.14 where the values offered
for each field do not get refreshed properly.

Installation
------------

1. Install the plugin jar file in jira.home/plugins/installed-plugins
2. Restart JIRA
3. Go to Admin, Plugins, MDSF and click Configure.

Add the Select Fields
---------------------

For each part of the MDSF field:

1. Add a Select Field for one part of the MDSF field. 
   Tip: naming these fields with a prefix of the MDSF field name they
   will be used can be helpful but is not necessary. For example,
   "Address City" is used by the Address MDSF field.
2. You can choose to have a searcher defined or not
3. Don't define more than one context for the new field
4. Don't add the new field to any screens. This is to avoid confusion
   between the MDSF field and this field.
5. Setting a default for the single context does not affect the MDSF field.

Add the MDSF Field
------------------

1. Create a new custom field of type MDSF
2. You can define more than one context for the new field, but test it
with one context first
3. Add the new field to a default screen

Note: one searcher is currently defined for MDSF fields. You can also
search using each of the dependent fields.

Configure the MDSF Field
------------------------

1. On the Custom Field screen, choose Configure for the new MDSF
   custom field, and then choose "Edit MDSF Dependent Fields"
2. Add the dependent fields in the desired order
3. Choose values for a combination and add it. It should appear as a
   valid combination. Note that "None" is a valid value for the a field
   even if the dependent field doesn't directly support it. The
   combination of all None is automatically added and means that no
   value is set in the MDSF field.
4. You can add more combinations of dependent fields at any time.
5. After you have defined some valid combinations, you can also choose
   one of them as the default value for the MDSF field.

Test the MDSF Field
-------------------

1. Create an issue and check that the default value is set for the MDSF field
2. Change the left-most field value of the MDSF field and check that
   the possible options of the other fields are updated to reflect the
   new value

Tips
----

The same select fields can be used in different MDSF fields.

Import Existing Combinations
----------------------------

The MDSF plugin can import data from Multi Level Cascade Select custom
fields (https://plugins.atlassian.com/plugin/details/5008). The MLCS
plugin does not support JIRA 4.x but its fields' options are preserved
during an update. There is no need to install the MLCS plugin in JIRA
4.x to use the data.

To use the existing options as MDSF combinations, first make sure that
the number of dependent fields in the MDSF field is the same as the
number in the MLCS field. This also assumes that you have set up the
dependent select fields ready to receive values. Then select a MLCS
custom field on the configuration page for the MDSF field.

Once the data in an MLCS field has been linked with an MDSF custom
field, it should be automatically converted to combinations for the
MDSF field. If an MLCS option has more levels that the current number of MDSF dependent fields, it is truncated.

 After that, the MLCS field's data could be removed, though
the data is only converted when each option is used once, so
rarely-used MLCS options might be lost.

The new combinations are only added to the MDSF field when the issues
with previous MLCS values are viewed. The recommended way to import many
options is to use the Issue Navigator to view all the issues
that might have had MLCS values, causing all the new combinations to
be populated.

Note that you should expect to see an error at start up in the JIRA
log file about the MLCS data being present without the MLCS
plugin. For example:

Could not load custom field type plugin with key 'com.sourcesense.jira.plugin.cascadingselect:multi-level-cascading-select'. Is the plugin present and enabled?

Once all the MLCS data has been imported into the MDSF plugin you can
safely remove the old custom fields from the database.

