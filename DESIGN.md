Design Notes
------------

Once a field is added, never want to have to read it because the id will change

Need to be able to select a field and add it to the MDSF field

Need to be able to reorder the dependent fields in the MDSF
field. This should reorder the display of valid combinations in the
configuration and in the user-visible view

Need to be able to delete a dependent field from the MDSF field. This
should remove the field from the valid combinations.

Need to only use the default (single?) context for a dependent field to avoid
illogical matches

Configuration UI
----------------

Add a custom field as usual, including contexts

Choose to configure MDSF fields

Select list of Custom Fields that implement MultipleSettableCustomFieldType (or just Select and not MDSF?) - Add Field

Display of dependent fields:

Field A Up/Down/Remove
Field B Up/Down/Remove
Field C Up/Down/Remove
...

Field A  Field B  Field C  - Add Combination

TODO could preserve the last values chosen here to make adding lots of options easier?

Can there be duplicates in an options list? Not allowed.

A combination is a text field with contents such as:

customfield_NNNN4:option_idA
customfield_NNNN1:option_idB
customfield_NNNN2:option_idC
customfield_NNNN3:option_idE

Display of combinations as a list sorted by the order of the options
in the dependent fields. Next to each combination is a Remove Combination button.


Can same option can appear in multiple combinations, e.g two Lincolns. Yes.

Can same field can appear more than once in an MDSF? Yes, but not sure if this is useful

User View and Edit
------------------

Display the select list in the specified order

When a select list is changed (chosen), update the choices in the
other select lists. If a value has already been chosen, don't update
it.

TODO test with importing and with multiple contexts


Expected behaviour
------------------

    // An option is marked as selected because it is what
    // is currently chosen in the field. The onChange function
    // means that just one field has been changed and all the other
    // select fields should have their options updated. 
    // Preserve the fact that the others were selected if possible.


If combos ar chosen from three fields, e.g.

A1, B1, None or
A1, B1, C1 or
A1, B1, C2
A1, B2, C3
A2, B2, C3

The current value is displayed. 

On changing one of the select fields, e.g.

A1, B1, C1 

to

A1, B2, C3

This happens by changing C1 to C3 which changes the fields to

A1, -1, C3

The choices are updated thus:

A - add A2, preserve A1
B - add nothing, nothing preserved


    // When all the fields are selected and I change one of the fields
    // to a new value, what is expected? Preserve the existing 
    // selections if possible, or clear all other fields and start again?
    // At the moment, the former is implemented.
    // If the existing selections cannot be preserved, then the default
    // option will be the first one, None. This makes sense unless there is
    // only one option to choose.

Options in 3 dependent fields:

Field A : A1, A2, A3, A4, A5
Field B : B1, B2, B3, B4, B5
Field C : C1, C2, C3, C4, C5

Combos specified for an MDSF field:

Field A     Field B    Field C

None        None       None
A1          None       None
A1          B1         None

A1          B1         C1
A1          B1         C2
A1          B1         C3
A1          B2         C1
A1          B2         C2
A1          B3         C3

A2          B1         C1
A2          B1         C2
A2          B1         C3
A2          B2         C1
A2          B2         C2
A2          B3         C3

A3          B1         C1
A3          B1         C2
A3          B1         C3
A3          B2         C1
A3          B2         C2
A3          B3         C3

Constraint Examples:

(A1,*,*)   B1,B2,B3     C1,C2,C3
(A1,B1,*)               C1,C2,C3
(A1,B1,C3)              C1,C2,C3

But we can't remove the options that allow changes to be made. So never remove the options in the first field


MLCS

Add the child option, then choose Edit for that option

Example:

Multi level options:

X
 X3
  X333
   X654
 X2 
 X4
Z
Y

mysql> select * from customfieldoption where customfield=10002;+-------+-------------+-------------------+----------------+----------+-------------+------------+
| ID    | CUSTOMFIELD | CUSTOMFIELDCONFIG | PARENTOPTIONID | SEQUENCE | customvalue | optiontype |
+-------+-------------+-------------------+----------------+----------+-------------+------------+
| 10012 |       10002 |             10012 |           NULL |        0 | X           | NULL       | 
| 10013 |       10002 |             10012 |           NULL |        2 | Y           | NULL       | 
| 10014 |       10002 |             10012 |           NULL |        1 | Z           | NULL       | 
| 10015 |       10002 |             10012 |          10012 |        0 | X3          | NULL       | 
| 10016 |       10002 |             10012 |          10012 |        1 | X2          | NULL       | 
| 10017 |       10002 |             10012 |          10012 |        2 | X4          | NULL       | 
| 10018 |       10002 |             10012 |          10015 |        0 | X333        | NULL       | 
| 10019 |       10002 |             10012 |          10018 |        0 | X654        | NULL       | 
+-------+-------------+-------------------+----------------+----------+-------------+------------+
8 rows in set (0.00 sec)

To create entries from this for MDSF, have to 

1. Check those values exist in the dependent custom fields
2. Enumerate the combos represented by this data
3. Create rows in customfieldoption with the field and option values and the correct sequence

Perhaps create the DAG then traverse it for enumeration?
Emit SQL to run to populate the database?

External Python app or a place to paste the data into JIRA?

Values in customfieldvalues

Select has the value in stringvalue

| 10002 | 10000 |       10000 | NULL      | B           |        NULL | NULL      | NULL      | NULL      | 

Cascading select has two rows, one for parent, one with parent key set
for child. The values are the ids of customfieldoption entries

| 10000 | 10000 |       10001 | NULL      | 10005       |        NULL | NULL      | NULL      | NULL      | 
| 10001 | 10000 |       10001 | 10005     | 10010       |        NULL | NULL      | NULL      | NULL      | 

MLCS have as many entries as there are in the chosen option, e.g.

| 10003 | 10000 |       10002 | 10018     | 10019       |        NULL | NULL      | NULL      | NULL      | 
| 10004 | 10000 |       10002 | 10015     | 10018       |        NULL | NULL      | NULL      | NULL      | 
| 10005 | 10000 |       10002 | NULL      | 10012       |        NULL | NULL      | NULL      | NULL      | 
| 10006 | 10000 |       10002 | 10012     | 10015       |        NULL | NULL      | NULL      | NULL      | 

To convert these values to an MDSF option, for each issue get the current value and find a combo that matches it and set it in the MDSF field

The transport object $value is a CustomFieldParams object. The view template
calls $value.getValuesForKey($key) which is from CollectionParams and
implemented in CustomFieldParamsImpl


Find the current values in the issue 10000
mysql> select issue, parentkey, stringvalue from customfieldvalue where customfield=10002 and issue=10000;
+-----------+-------------+
| Parentkey | stringvalue |
+-----------+-------------+
| 10018     | 10019       | 
| 10015     | 10018       | 
| NULL      | 10012       | 
| 10012     | 10015       | 
+-----------+-------------+

mysql> select id, customvalue from customfieldoption where id in (10019, 10018, 10012, 10015);
+-------+-------------+
| Id    | customvalue |
+-------+-------------+
| 10012 | X           | 
| 10015 | X3          | 
| 10018 | X333        | 
| 10019 | X654        | 
+-------+-------------+

Can't assume id order?
MLCS data persists through an XML upgrade even if it is no longer used

Field gets added again when reloading the screen

8/30 
Fixed two open select elements in edit-config.vm that only showed up in Firefox
Reproduced screenshot from Kiran J
Fixed two instances of using a js array siblings as a function. Works in chrome, not in Firefox

9/8

Existing Empty, Default and Required Behaviors
----------------------------------------------

For a regular select field.

customfield - the custom field definition table
customfieldoption - for custom fields that have options
customfieldvalue - the value of custom fields in each issue

When a select field is created, there are no options defined and no
values for any issues. If the field appears on a screen, then it only
has one value: None (-1). Once options are defined, None is still the
first choice. None is automatically defined for all select fields.

If the field is marked as required in a Field Configuration then
creating or editing an issue has the error that the field is required.

If the field is not required, then saving it with None (-1) will not
produce an entry in the customfieldvalue table. None is just a way for
JIRA to detect required or optional.

If options are now defined for the custom field and if the field is
required, the None option is still offered and will still result in an
error.

The default value can be not set, in which case None will be used. If
it is set to an option (or None), then that choice will be preselected
for the field.

Summary: None is always an option, but will result in an error if the
field is required. Default is what is preselected, or None if no default is set.

MDSF Empty, Default and Required Behaviors
------------------------------------------

When no dependent fields are defined, should appear to user as either
blank or "No fields configured".

When fields are defined but no combos, should appear to user as None,
 None, None where the number matches the number of fields

When field is required, then a combo other than None, None, None must appear

When default is defined, then the preselected combo is the default one

When creating an issue and there are no field, field value should be empty 

None may be part of a valid combo

How to indicate that the field is empty. None is used for
dependent fields. Ans: all Nones but don't let all Nones be added as a
combo

Test Cases:

No Fields - blank

One Field, no Combos - None
One Field, one Combo - None, combo value
One Field, two Combos - None, combo values

Two Fields, no Combos - None, None
Two Fields, one Combo - None, combo value
Two Fields, two Combos - None, combo values

Contexts
--------

TODO one MDSF field with multiple contexts - should be different
fields or just different combos. Want a way to copy combos from
another context.

i18n

Done: MdsfCFType, .vm files
Not done yet: MdsfConfigItem, MdsfDAO
Complex to access the plugin_i18n object, perhaps put it in MdsfDAO?
